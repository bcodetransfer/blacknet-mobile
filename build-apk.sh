## 计时
SECONDS=0

app="app"

echo "清理 build"
find . -d -name "build" | xargs rm -rf
flutter clean
rm -rf build
rm -rf app

echo "开始获取 packages 插件资源"
flutter packages get

mkdir app

#==================================apk==================================
echo "开始打包apk"
flutter build apk --release #--no-codesign 
echo "打包apk已完成"

cp -r build/app/outputs/apk/release/app-release.apk app/blacknet-mobile-1.2.4.apk

open app

#=========