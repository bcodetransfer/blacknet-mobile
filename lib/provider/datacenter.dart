import 'package:blacknet/components/api/api.dart';
import 'package:blacknet/components/index.dart';
import 'package:blacknet/models/index.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/res/index.dart';
import 'package:blacknet/services/LocalAuthenticationService.dart';
import 'package:blacknet/services/service_locator.dart';
import 'package:blacknet/utils/toast.dart';
import 'package:flutter/material.dart';
import 'package:blacknet/models/bln.dart';
import 'package:provider/provider.dart';

import 'package:blacknet_lib/blacknet.dart';

class DataCenterProvider with ChangeNotifier {
  final LocalAuthenticationService _localAuth =
      locator<LocalAuthenticationService>();

  List<BlnTxns> allTypeTransactions;
  List<BlnTxns> transferTransactions;
  List<BlnTxns> leaseTransactions;
  List<BlnTxns> thePoSTransactions;
  List<BlnLease> outleases = [];
  BlnBalance balance;
  Hashrate hashrate = Hashrate(networkhashrate: 0, workers: 0);
  PoolHashrate poolHashrate = PoolHashrate(hashrate: 0, number: 0, blockHeight: 0);
  PosWorker posWorker = PosWorker(hashrate: 0, totalReward: 0, paid: 0, payouts: 0);
  List<PosListItem> thePoSlist;
  String address;

  List<BlnTxns> tempTransactions;


  DataCenterProvider() {
    allTypeTransactions = [];
    transferTransactions = [];
    leaseTransactions = [];
    thePoSTransactions = [];
    outleases = [];
    thePoSlist = [];
    tempTransactions = [];
  }

  Future<void> initData(addr) async {
    var futures = <Future>[];
    address = addr;

    allTypeTransactions = [];
    transferTransactions = [];
    leaseTransactions = [];
    thePoSTransactions = [];
    outleases = [];
    thePoSlist = [];
    tempTransactions = [];
    balance = BlnBalance.empty();


    futures.addAll([
      Api.getTxns(address, 1),
      Api.getTxns(address, 1, 0),
      Api.getTxns(address, 1, 2),
      Api.getTxns(address, 1, 254),
      Api.getBlance(address)
    ]);

    var res = await Future.wait(futures);

    allTypeTransactions = res[0];
    transferTransactions = res[1];
    leaseTransactions = res[2];
    thePoSTransactions = res[3];
    balance = res[4];
    notifyListeners();
    asyncData();
  }

  Future<void> asyncData() async {
    var futures = <Future>[];
    futures.addAll([
      Api.outLeases(address),
      Api.getHashrate(),
      Api.getPoolHashrate(),
      Api.getPosWorker(address),
      Api.getPosLists(address)
    ]);

    var res = await Future.wait(futures);

    outleases = res[0];
    hashrate = res[1];
    poolHashrate = res[2];
    posWorker = res[3];
    thePoSlist = res[4];
    notifyListeners();
  }

  Future<void> refershBalance() async {
    balance = await Api.getBlance(address);
  }

  Future<void> refershAllTxns() async {
    allTypeTransactions = await Api.getTxns(address, 1);
  }

  void addTxToTemp(BlnTxns tx) {
    tempTransactions.insert(0, tx);
  }

  double getBalanceWithOffset(){
    return balance.balance + getBalanceOffset();
  }

  double getBalanceOffset(){

    double offset = 0;

    tempTransactions.forEach((tx){
      if(tx.type == TxType.Transfer || tx.type == TxType.Lease){
        offset -= tx.data.amount;
      }
      if(tx.type == TxType.CancelLease){
        offset += tx.data.amount;
      }
    });
    return offset;
  }

  List<BlnTxns> getTxns(type) {
    var res = [];

    tempTransactions.forEach((tx){
      if(tx.type == type){
        res.add(tx);
      }
    });

    if (type == 0) {
      res = transferTransactions;
    } else if (type == 2) {
      res = leaseTransactions;
    } else if (type == 254) {
      res = thePoSTransactions;
    } else {
      res = allTypeTransactions;
    }
    return res;
  }

  Future<void> refershTxnsByType(type) async {
    var futures = <Future>[];

    futures.add(Api.getBlance(address));
    if (type != null)
      futures.add(Api.getTxns(address, 1, type));
    else
      futures.add(Api.getTxns(address, 1));

    var res = await Future.wait(futures);
    
    balance = res[0];
    
    List<BlnTxns> newTempTxs = [];
    tempTransactions.forEach((tx){
      bool isconfirmed = false;
      res[1].forEach((transaction){
        if(tx.hash == transaction.txid){
          isconfirmed = true;
        }
      });
      if(!isconfirmed){
        newTempTxs.add(tx);
      }
    });
    tempTransactions = newTempTxs;

    res[1] = tempTransactions + res[1];

    switch (type) {
      case 0:
        transferTransactions = res[1];
        break;
      case 2:
        leaseTransactions = res[1];
        break;
      case 254:
        thePoSTransactions = res[1];
        break;
      default:
        allTypeTransactions = res[1];
    }
  }

  Future<void> refreshPoSState() async {
    var futures = <Future>[];
    futures.addAll([
      Api.getHashrate(),
      Api.getPoolHashrate(),
      Api.getPosWorker(address),
      Api.getPosLists(address)
    ]);

    var res = await Future.wait(futures);

    hashrate = res[0];
    poolHashrate = res[1];
    posWorker = res[2];
    thePoSlist = res[3];
  }

  List<BlnTxns> getAllTypeTransactions() {
    return allTypeTransactions;
  }

  void setAllTypeTransactions(List<BlnTxns> allTypeTransactions) {
    allTypeTransactions = allTypeTransactions;
  }

  List<BlnTxns> getTransferTransactions() {
    return transferTransactions;
  }

  void setTransferTransactions(List<BlnTxns> transferTransactions) {
    transferTransactions = transferTransactions;
  }

  List<BlnTxns> getLeaseTransactions() {
    return leaseTransactions;
  }

  void setLeaseTransactions(List<BlnTxns> leaseTransactions) {
    leaseTransactions = leaseTransactions;
  }

  List<BlnTxns> getThePoSTransactions() {
    return thePoSTransactions;
  }

  void setThePoSTransactions(List<BlnTxns> thePoSTransactions) {
    thePoSTransactions = thePoSTransactions;
  }

  BlnBalance getBalance() {
    return balance;
  }

  void setBalance(BlnBalance balance) {
    balance = balance;
  }

  Hashrate getHashrate() {
    return hashrate;
  }

  void setHashrate(Hashrate hashrate) {
    hashrate = hashrate;
  }

  PoolHashrate getPoolHashrate() {
    return poolHashrate;
  }

  void setPoolHashrate(PoolHashrate poolHashrate) {
    poolHashrate = poolHashrate;
  }

  PosWorker getPosWorker() {
    return posWorker;
  }

  void setPosWorker(PosWorker posWorker) {
    posWorker = posWorker;
  }

  List<PosListItem> getThePoSlist() {
    return thePoSlist;
  }

  void setThePoSlist(List<PosListItem> thePoSlist) {
    thePoSlist = thePoSlist;
  }

  bool verifyMessage(signature, message, [String text]) {
    var str = address;
    if (text.length == 64) str = text;
    return Blacknet.verify(str, signature.trim(), message.trim());
  }

  leaseAllCoinsToStakePool(context) async {
    await refershBalance();
    var coins = balance.confirmedBalance;
    var poolAddress =
        'blacknet126avvjlevw9xqnmlgscytxtuf57ndwzmz569vcaatceevhcjxjfq29ywqc';

    // 10 BLN for prepare fee of cancel lease
    var amount = (coins - 10*1e8 - 100000) / 1e8;

    return await leaseCoins(context, amount, poolAddress);
  }

  Future<String> leaseCoins(context, amount, to) async {
    return await sendCoins('lease', context, amount, to);
  }

  Future<String> transferCoins(context, amount, to, encrypted, msg) async {
    return await sendCoins('transfer', context, amount, to, encrypted, msg);
  }

  Future<String> cancelLeasesCoins(context, amount, to, encrypted, msg) async {
    return await sendCoins('cancellease', context, amount, to, encrypted, msg);
  }

  Future<String> sendCoins(String type, context, amount, to,
      [bool encrypted, String msg, int height]) async {
    var textMsg = 'loading';
    switch (type) {
      case 'lease':
        textMsg = 'Leasing';
        break;
      case 'transfer':
        textMsg = 'Transfering';
        break;
      case 'cancellease':
        textMsg = 'Cancelling';
        break;
    }

    bool checkResult = await _localAuth.auth(textMsg);

    if (checkResult) {
      Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
      double fee = 0.001;
      var mnemonic = await bln.getMnemonic();
      var res;

      if (type == 'lease') {
        res = await Api.lease(mnemonic, fee, amount, to);
      }

      if (type == 'transfer') {
        res = await Api.transfer(mnemonic, fee, amount, to, encrypted, msg);
      }

      if (type == 'cancellease') {
        res = await Api.cancelLease(mnemonic, fee, amount, to, height);
      }

      dismissProgressDialog();
      if (res.code == 200) {
        Toast.show('$type success! ', duration: 2000);
        return res.body;
      }
      Toast.show('$type failed: ${res.body}', duration: 2000);
      return 'false';
    } else {
      dismissProgressDialog();
      return 'false';
    }
  }
}
