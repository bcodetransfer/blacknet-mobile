

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomLayout{



  static Widget buttonLayout (Widget child){
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16),
      child: Container(margin: const EdgeInsets.only(top: 25),child:child)
    );
  }

  static Widget transferPageLayout(List<Widget> child){
    
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: child
        )
    );
  }

  static Widget formLayout(key, child){
    return Expanded(
        flex: 1,
        child: SingleChildScrollView(
          key: key,
          // padding: const EdgeInsets.symmetric(vertical: 16.0),
          physics: BouncingScrollPhysics(),
          child:child));
  }

}