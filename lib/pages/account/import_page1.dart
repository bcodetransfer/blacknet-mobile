import 'package:flutter/material.dart';

import 'package:blacknet/pages/account/presenter/import_presenter.dart';

import 'package:blacknet/common/index.dart';


class AccountImportPage1 extends StatefulWidget {
  @override
  AccountImportPageState1 createState() => AccountImportPageState1();
}

class AccountImportPageState1 extends BasePageState<AccountImportPage1, AccountImportPagePresenter1> {

  @override
  AccountImportPagePresenter1 createPresenter() {
    return AccountImportPagePresenter1();
  }

  final TextEditingController _textController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _nextStepStatus = false;
  int _wordsLen = 0;
  List<Bln> _blns = List();

  @override
  void initState() {
    super.initState();
    // init form listener
    _textController.addListener((){
      setState(() {
        _wordsLen = _textController.text != "" && _textController.text != null ? _textController.text.split(" ").length : 0;
        _nextStepStatus = _wordsLen == 12;
      });
    });
    // accounts
    _blns = SpHelper.getBlnList(Constants.accountList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min, 
            children: <Widget>[
              getFormBox(),
              getFormBottm()
            ]
          )
        )
      )
    );
  }

  Widget getFormBox(){
    return Expanded(
      flex: 1, child: 
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          ListTile(
            title: Center(
              child: Text(S.of(context).importInputMnemonic,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold))
            )
          ),
          Center(
            child: TextFormField(
              autofocus: true,
              controller: _textController,
              decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(20),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none
              ),
              maxLines: 2,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              obscureText: false,
              cursorWidth: 2,
              cursorColor: Theme.of(context).accentColor
            )
          ),
          ListTile(
            title: tipsWordLen(_wordsLen),
            subtitle: Text(S.of(context).importInputMnemonicDesc, 
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14)
            ),
          )
        ]
      )
    );
  }

  bool validator(){
    return Bln.checkMnemonic(_textController.text);
  }

  String checkAddress(String address){
    var text;
    _blns.forEach((bln){
      if (bln.address == address) {
        text = S.of(context).accountExists;
      }
    });
    return text;
  }

  Widget getFormBottm(){
    Color color = Theme.of(context).appBarTheme.actionsIconTheme.color;
    Function onTap;
    if (_nextStepStatus) {
      color = Theme.of(context).accentColor;
      onTap = () async {
        if (validator()) {
          String mnemonic = _textController.text;
          showProgress(true);
          Bln bln = await createBlnByMnemonic(mnemonic);
          closeProgress();
          String err = checkAddress(bln.address);
          if (err != null) {
            showToast(err);
          }else{
            OkNavigator.push(context, '${AccountRouter.accountImportPage2}?mnemonic=${mnemonic}&address=${bln.address}');
          }
        } else {
          showWarning();
        }
      };
    }
    Widget trailing = Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                S.of(context).nextStep,
                style: Theme.of(context).textTheme.button.apply(
                  color: color,
                ), 
                textAlign: TextAlign.center,
              ),
              Icon(Icons.navigate_next, color: color)
            ]
          ),
          onTap: onTap
        )
      ]
    );
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Theme.of(context).backgroundColor, width: 1)
        )
      ),
      child: ListTile(
        title: trailing
      ),
    );
  }

  Widget tipsWordLen([int len]){
    if (len != null && len > 0) {
      var color;
      if (len == 12) {
        color = Colours.gold;
      }
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            S.of(context).wordLen+": ",
            textAlign: TextAlign.center,
          ),
          Text(
            '${len} '+ S.of(context).word,
            style: TextStyle(
              color: color, 
              fontWeight: FontWeight.bold
            ), 
            textAlign: TextAlign.center,
          ),
        ]
      );
    }
    return Text("", 
          textAlign: TextAlign.center,
          style: TextStyles.textError14
        );
  }
}