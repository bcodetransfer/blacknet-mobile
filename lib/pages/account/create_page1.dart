import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';
import 'package:blacknet/pages/account/router.dart';
import 'package:blacknet/pages/account/presenter/create_presenter.dart';


class AccountCreatePage1 extends StatefulWidget {
  @override
  AccountCreatePageState1 createState() => AccountCreatePageState1();
}

class AccountCreatePageState1 extends BasePageState<AccountCreatePage1, AccountCreatePagePresenter1> {

  @override
  AccountCreatePagePresenter1 createPresenter() {
    return AccountCreatePagePresenter1();
  }

  final TextEditingController _nameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _nextStepStatus = false;
  Widget _errorText = Text("", style: TextStyles.textError14);
  List<Bln> _blns = List();

  @override
  void initState() {
    super.initState();
    // init form listener
    _nameController.addListener((){
      setState(() {
        _nextStepStatus = _nameController.text.length > 0 && _formKey.currentState.validate();
        _errorText = errorText(null);
      });
    });
    // accounts
    _blns = SpHelper.getBlnList(Constants.accountList);
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor,
        // centerTitle: false,
        // automaticallyImplyLeading: false,
        // title: getCancelButton()
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min, 
            children: <Widget>[
              getFormBox(),
              getFormBottm()
            ]
          )
        )
      )
    );
  }

  Widget getCancelButton(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
          child: new Center(
            child: new Text(
              S.of(context).cancel,
              style: Theme.of(context).textTheme.button.apply(
                color: Theme.of(context).appBarTheme.actionsIconTheme.color,
              ), 
              textAlign: TextAlign.center,
            ),
          ),
          onTap: () async {
            if (await showConfirmDialog(
              Text(S.of(context).accountCreateConfirmTitle),
              Text(S.of(context).accountCreateConfirmContent)
            )) {
              OkNavigator.goBack(context);
            }
          }
        )
      ]
    );
  }

  Widget getFormBox(){
    return Expanded(
      flex: 1, child: 
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          ListTile(
            title: Text(S.of(context).accountCreateName, 
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)
            )
          ),
          ListTile(
            title: Text(S.of(context).accountCreateNameDesc, 
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14)
            )
          ),
          SizedBox(height: ThemeLayout.height(20)),
          ListTile(
            title: TextFormField(
              controller: _nameController,
              autofocus: true,
              decoration: InputDecoration(
                contentPadding: ThemeLayout.padding(15, 15, 15, 15),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(color: Theme.of(context).backgroundColor)
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(color: Theme.of(context).backgroundColor)
                ),
                fillColor: Theme.of(context).backgroundColor, 
                filled: true,
                hintText: S.of(context).accountCreateNameHint,
                hintStyle: TextStyle(
                  color: Theme.of(context).primaryColorLight
                )
              ),
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20.0),
              obscureText: false,
              cursorWidth: 2,
              cursorColor: Theme.of(context).accentColor,
              validator: LengthRangeValidator(min: 0, max: 32, errorText: S.of(context).accountCreateNameHint)
            ),
            subtitle: Container(
              margin: ThemeLayout.marginTop(10),
              child: Center(
                child: _errorText
              )
            )
          )
      ])
    );
  }

  bool validator(){
    var text;
    _blns.forEach((bln){
      if (bln.name == _nameController.text || _nameController.text == null) {
        text = S.of(context).accountNameExists;
      }
    });
    if (text != null) {
      setState(() {
        _errorText = errorText(text);
      });
      return false;
    }
    return true;
  }

  Widget getFormBottm(){
    Color color = Theme.of(context).appBarTheme.actionsIconTheme.color;
    Function onTap;
    if (_nextStepStatus) {
      color = Theme.of(context).accentColor;
      onTap = (){
        if (validator()) {
          OkNavigator.push(context, '${AccountRouter.accountCreatePage2}?name=${_nameController.text}');
        }
      };
    }
    Widget trailing = Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                S.of(context).nextStep,
                style: Theme.of(context).textTheme.button.apply(
                  color: color,
                ), 
                textAlign: TextAlign.center,
              ),
              Icon(Icons.navigate_next, color: color)
            ]
          ),
          onTap: onTap
        )
      ]
    );
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Theme.of(context).backgroundColor, width: 1)
        )
      ),
      child: ListTile(
        title: trailing
      ),
    );
  }

}