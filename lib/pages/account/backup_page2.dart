import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';
import 'package:blacknet/pages/account/router.dart';
import 'package:blacknet/pages/account/presenter/backup_presenter.dart';


class AccountBackupPage2 extends StatefulWidget {
  const AccountBackupPage2({
    Key key,
    @required this.name
  }) : super(key: key);
  final String name;
  @override
  AccountBackupPageState2 createState() => AccountBackupPageState2();
}

class AccountBackupPageState2 extends BasePageState<AccountBackupPage2, AccountBackupPagePresenter2> {

  @override
  AccountBackupPagePresenter2 createPresenter() {
    return AccountBackupPagePresenter2();
  }

  final TextEditingController _textController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _nextStepStatus = false;

  @override
  void initState() {
    super.initState();
    // init form listener
    _textController.addListener((){
      setState(() {
        _nextStepStatus = _textController.text.length > 5;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min, 
            children: <Widget>[
              getFormBox(),
              getFormBottm()
            ]
          )
        )
      )
    );
  }

  Widget getFormBox(){
    return Expanded(
      flex: 1, child: 
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          ListTile(
            title: Center(
              child: Text(S.of(context).settingPassowrd, 
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold))
            )
          ),
          ListTile(
            title: TextFormField(
              autofocus: true,
              controller: _textController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none
              ),
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30.0),
              obscureText: true,
              cursorWidth: 2,
              cursorColor: Theme.of(context).accentColor
            ),
            subtitle: errorText(null),
          ),
          Container(
            margin: ThemeLayout.marginBottom(10),
            child: ListTile(
              title: Text(S.of(context).accountCreatePasswordDesc, 
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14)
              )
            )
          )
        ]
      )
    );
  }

  Widget getFormBottm(){
    Color color = Theme.of(context).appBarTheme.actionsIconTheme.color;
    Function onTap;
    if (_nextStepStatus) {
      color = Theme.of(context).accentColor;
      onTap = (){
        OkNavigator.push(context, '${AccountRouter.accountCreatePage3}?name=${widget.name}&password=${_textController.text}');
      };
    }
    Widget trailing = Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                S.of(context).nextStep,
                style: Theme.of(context).textTheme.button.apply(
                  color: color,
                ), 
                textAlign: TextAlign.center,
              ),
              Icon(Icons.navigate_next, color: color)
            ]
          ),
          onTap: onTap
        )
      ]
    );
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Theme.of(context).backgroundColor, width: 1)
        )
      ),
      child: ListTile(
        title: trailing
      ),
    );
  }
}