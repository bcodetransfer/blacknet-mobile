export 'package:blacknet/pages/account/create_page1.dart';
export 'package:blacknet/pages/account/create_page2.dart';
export 'package:blacknet/pages/account/create_page3.dart';
export 'package:blacknet/pages/account/backup_page.dart';

export 'package:blacknet/pages/account/recover_page.dart';
export 'package:blacknet/pages/account/recover_page2.dart';
export 'package:blacknet/pages/account/recover_page3.dart';