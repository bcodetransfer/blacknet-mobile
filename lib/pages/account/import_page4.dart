import 'package:flutter/material.dart';

import 'package:blacknet/pages/account/presenter/import_presenter.dart';
import 'package:blacknet/common/index.dart';
import 'package:provider/provider.dart';


class AccountImportPage4 extends StatefulWidget {
  const AccountImportPage4({
    Key key,
    @required this.mnemonic,
    @required this.name,
    @required this.password,
    @required this.address
  }) : super(key: key);
  final String mnemonic;
  final String name;
  final String password;
  final String address;
  @override
  AccountImportPageState4 createState() => AccountImportPageState4();
}

class AccountImportPageState4 extends BasePageState<AccountImportPage4, AccountImportPagePresenter4> {

  @override
  AccountImportPagePresenter4 createPresenter() {
    return AccountImportPagePresenter4();
  }

  final TextEditingController _textController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _nextStepStatus = false;
  Widget _errorText = Text("", style: TextStyles.textError14);

  @override
  void initState() {
    super.initState();
    // init form listener
    _textController.addListener((){
      setState(() {
        _nextStepStatus = _textController.text.length > 5;
        _errorText = errorText(null);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min, 
            children: <Widget>[
              getFormBox(),
              getFormBottm()
            ]
          )
        )
      )
    );
  }

  Widget getFormBox(){
    return Expanded(
      flex: 1, child: 
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          ListTile(
            title: Center(
              child: Text(S.of(context).confirmPassowrd,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold))
            )
          ),
          ListTile(
            title: TextFormField(
              autofocus: true,
              controller: _textController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none
              ),
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30.0),
              obscureText: true,
              cursorWidth: 2,
              cursorColor: Theme.of(context).accentColor,
            ),
            subtitle: _errorText,
          ),
          Container(
            margin: ThemeLayout.marginBottom(10),
            child: ListTile(
              title: Text(S.of(context).accountCreatePasswordDesc,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14)
              )
            )
          )
        ]
      )
    );
  }

  bool validator(){
    var text = MatchValidator(errorText: S.of(context).inconsistentPassowrd).validateMatch(_textController.text, widget.password);
    if (text != null) {
      setState(() {
        _errorText = errorText(text);
      });
      return false;
    }
    return true;
  }

  Widget getFormBottm(){
    Color color = Theme.of(context).appBarTheme.actionsIconTheme.color;
    Function onTap;
    if (_nextStepStatus) {
      color = Theme.of(context).accentColor;
      onTap = () async {
        if (validator()) {
          showProgress(true);
          Bln bln = Bln.create(widget.name, widget.password, widget.mnemonic, widget.address);
          Provider.of<AccountProvider>(context).setAccount(bln);
          Provider.of<AccountProvider>(context).setCurrentBln(bln);
          closeProgress();
          // navgation
          // go to wallet index page
          OkNavigator.push(context, Routes.mainPage);
        }
      };
    }
    Widget trailing = Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                S.of(context).nextStep,
                style: Theme.of(context).textTheme.button.apply(
                  color: color,
                ), 
                textAlign: TextAlign.center,
              ),
              Icon(Icons.navigate_next, color: color)
            ]
          ),
          onTap: onTap
        )
      ]
    );
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Theme.of(context).backgroundColor, width: 1)
        )
      ),
      child: ListTile(
        title: trailing
      ),
    );
  }

}