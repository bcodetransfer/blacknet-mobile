
import 'package:fluro/fluro.dart';

import 'package:blacknet/routers/router_init.dart';

// wallet pages
import 'create_page1.dart';
import 'create_page2.dart';
import 'create_page3.dart';
import 'unlock_page.dart';
import 'import_page1.dart';
import 'import_page2.dart';
import 'import_page3.dart';
import 'import_page4.dart';

class AccountRouter implements IRouterProvider{
  
  static String accountCreatePage1 = "/account/createpage1";
  static String accountCreatePage2 = "/account/createpage2";
  static String accountCreatePage3 = "/account/createpage3";
  static String accountImportPage1 = "/account/importpage1";
  static String accountImportPage2 = "/account/importpage2";
  static String accountImportPage3 = "/account/importpage3";
  static String accountImportPage4 = "/account/importpage4";
  static String accountUnlock = "/account/unlock";
  
  @override
  void initRouter(Router router) {
    // account unlock
    router.define(accountUnlock, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new AccountUnlockPage(
        address: params['address']?.first,
        accountName: params['name']?.first,
      );
    }));
    // wallet
    router.define(accountCreatePage1, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return AccountCreatePage1();
    }));
    router.define(accountCreatePage2, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new AccountCreatePage2(
        name: params['name']?.first
      );
    }));
    router.define(accountCreatePage3, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new AccountCreatePage3(
        name: params['name']?.first,
        password: params['password']?.first,
      );
    }));
    router.define(accountImportPage1, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return AccountImportPage1();
    }));
    router.define(accountImportPage2, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new AccountImportPage2(
        mnemonic: params['mnemonic']?.first,
        address: params['address']?.first
      );
    }));
    router.define(accountImportPage3, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new AccountImportPage3(
        mnemonic: params['mnemonic']?.first,
        name: params['name']?.first,
        address: params['address']?.first
      );
    }));
    router.define(accountImportPage4, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new AccountImportPage4(
        mnemonic: params['mnemonic']?.first,
        name: params['name']?.first,
        password: params['password']?.first,
        address: params['address']?.first
      );
    }));
    
  }
}