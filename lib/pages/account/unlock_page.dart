import 'package:blacknet/utils/index.dart';
import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';
import 'package:blacknet/pages/account/presenter/unlock_presenter.dart';

class AccountUnlockPage extends StatefulWidget {
  const AccountUnlockPage({
    Key key,
    @required this.address,
    @required this.accountName,
  }) : super(key: key);
  final String address;
  final String accountName;
  @override
  AccountUnlockPageState createState() => AccountUnlockPageState();
}

class AccountUnlockPageState extends BasePageState<AccountUnlockPage, AccountUnlockPagePresenter> {

  @override
  AccountUnlockPagePresenter createPresenter() {
    return AccountUnlockPagePresenter();
  }

  Bln bln;
  final TextEditingController _textController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _nextStepStatus = false;
  Widget _errorText = Text("", style: TextStyles.textError14);

  @override
  void initState() {
    super.initState();
    // face ID
    Future.delayed(Duration(milliseconds: 300)).then((res) async {
      bln = Provider.of<AccountProvider>(context).getAccount(widget.address);
      if (bln != null && !bln.isEmpty() && bln.isLocalAuth()) {
         if (await showLocalAuth()) {
           OkNavigator.push(context, Routes.mainPage);
         }
      }
    });
    // init form listener
    _textController.addListener((){
      setState(() {
        _nextStepStatus = _textController.text.length > 5;
        _errorText = errorText(null);
      });
    });
  }

  bool _checked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min, 
            children: <Widget>[
              getFormBox(),
              getFormBottm()
            ]
          )
        )
      )
    );
  }

  Widget getFormBox(){
    return Expanded(flex: 1, child: 
      CustomScrollView(
        slivers: <Widget>[
          SliverList(delegate: SliverChildListDelegate([
            ListTile(
              title: Center(
                child: Container(
                  width: ThemeLayout.width(60),
                  height: ThemeLayout.height(60),
                  child: new CircleAvatar(
                    backgroundImage: AssetImage('assets/images/1024.png'),
                  )
                )
              )
            ),
            ListTile(
              title: Center(
                child: Text(widget.accountName, 
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))
              ),
              subtitle: Center(
                child: Text(shortAddress(widget.address, 20))
              )
            ),
            ListTile(
              title: TextFormField(
                autofocus: true,
                controller: _textController,
                decoration: InputDecoration(
                    contentPadding: ThemeLayout.padding(15, 15, 15, 15),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide(color: Theme.of(context).backgroundColor)
                    ),
                    fillColor: Theme.of(context).backgroundColor, 
                    filled: true,
                    hintText: S.of(context).inputPassword,
                    hintStyle: TextStyle(
                      color: Theme.of(context).primaryColorLight
                    )
                ),
                maxLines: 1,
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                ),
                obscureText: true,
                cursorWidth: 2,
                cursorColor: Theme.of(context).accentColor,
              )
            ),
            Container(
              padding: EdgeInsets.only(left: 16, right: 16),
              child: getFaceAndTips()
            )
          ]))
        ]
      )
    );
  }

  Widget getFaceAndTips(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(flex: 1, child: 
          CheckboxListTile(
            title: Text(S.of(context).savePassword),
            value: _checked,
            activeColor: Theme.of(context).accentColor,
            onChanged: (bool checked) async{
              String type = await getBiometricType();
              if (type != null) {
                bool forbid = await showLocalAuthToggle(!checked);
                if (checked) {
                  // allow
                  if (!forbid) {
                    checked = false;
                  }else{
                    // allow
                    Log.e("showLocalAuth");
                    bool allow = await showLocalAuth();
                    Log.e(allow.toString());
                    if (allow) {
                      bln.localAuth = true;
                    }
                  }
                }else{
                  // forbid
                  if (!forbid) {
                    checked = true;
                  }
                }
              }else{
                // cannot set faceid touchid
                // input password
                checked = false;
                Log.e("password");
              }
              setState(() {
                _checked = checked;
              });
            },
            controlAffinity: ListTileControlAffinity.leading
          )
        ),
        Expanded(flex: 1, child: Align(
          alignment: Alignment.centerRight,
          child: _errorText
        ))
      ]
    );
  }

  bool validator(){
    if (_textController.text != bln.password) {
      setState(() {
        _errorText = errorText(S.of(context).validationPassowrd);
      });
      return false;
    }
    return true;
  }

  Widget getFormBottm(){
    Color color = Theme.of(context).accentColor;
    Function onTap;
    if (_nextStepStatus) {
      onTap = () async{
        if (validator()) {
          bln.localAuth = _checked;
          Provider.of<AccountProvider>(context).setAccount(bln);
          Provider.of<AccountProvider>(context).setCurrentBln(bln);
          // go to wallet index page
          OkNavigator.push(context, Routes.mainPage);
        }
      };
    }
    Widget trailing = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
          child: Text(
            S.of(context).importMnemonic,
            style: Theme.of(context).textTheme.button.apply(
              color: color,
            ),
            textAlign: TextAlign.center,
          ),
          onTap: (){
            showImportButtonMenu();
          }
        ),
        RaisedButton(
          child: Text(S.of(context).done),
          onPressed: onTap
        )
      ]
    );
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Theme.of(context).backgroundColor, width: 1)
        )
      ),
      child: ListTile(
        title: trailing
      ),
    );
  }
}