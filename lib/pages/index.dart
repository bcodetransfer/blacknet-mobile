export 'package:blacknet/pages/account/index.dart';
export 'package:blacknet/pages/home/index.dart';
export 'package:blacknet/pages/theme/index.dart';
export 'package:blacknet/pages/me/index.dart';

import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:blacknet/widgets/index.dart';

import 'index_presenter.dart';

class IndexPage extends StatefulWidget {
  @override
  IndexPageState createState() => IndexPageState();
}

class IndexPageState extends BasePageState<IndexPage, IndexPagePresenter> {

  String testAccount = "blacknet14w6tm25y7rt24zj7r8fq7rnzd50qtpgmpfwv50r7qjnqhcwlxszqanh036";
  
  @override
  IndexPagePresenter createPresenter() {
    return IndexPagePresenter();
  }

  @override
  void initState() {
    super.initState();

    // 存在当前用户 直接跳转
    Future.delayed(Duration(milliseconds: 300)).then((res){
      Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
      if (bln != null && !bln.isEmpty()) {
        OkNavigator.push(context, "${AccountRouter.accountUnlock}?address=${bln.address}&name=${bln.name}");
      }else{
        // OkNavigator.pushReplacementNamed(context, Routes.index);
      }
    });
  }
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 414, height: 896, allowFontScaling: true)
          ..init(context);
    return Scaffold(
      appBar: my_appBar(
        Text(S.of(context).selectAccount, style: TextStyle(fontSize: 24)),
        Theme.of(context).backgroundColor,
        Theme.of(context).canvasColor,
        [
          IconButton( // action button
            icon: new Icon(Icons.more_horiz),
            onPressed: () {
              showImportButtonMenu();
            },
          )
        ]
      ),
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min, 
          children: <Widget>[
            SizedBox(height: ThemeLayout.height(15)),
            getAccountBox(),
            getFormBottm(),
          ]
        )
      )
    );
  }

  Widget getFormBottm(){
    Widget trailing = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                S.of(context).craeteWallet,
                style: Theme.of(context).textTheme.button.apply(
                  color: Theme.of(context).accentColor,
                ), 
                textAlign: TextAlign.center,
              )
            ]
          ),
          onTap: (){
            OkNavigator.push(context, AccountRouter.accountCreatePage1);
          }
        )
      ]
    );
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Theme.of(context).backgroundColor, width: 1)
        )
      ),
      child: ListTile(
        title: trailing
      ),
    );
  }

  SliverAppBar getSliverAppBar() {
    return SliverAppBar(
      title: Text(S.of(context).selectAccount, style: TextStyle(fontSize: 24)),
      backgroundColor: Theme.of(context).canvasColor,
      leading: Gaps.empty,
      elevation: 1.0,
      expandedHeight: ThemeLayout.height(70),
      pinned: true, // 固定在顶部
    );
  }

  Widget getAccountBox(){
    return Expanded(flex: 1, child: 
      CustomScrollView(
        slivers: <Widget>[
          // getSliverAppBar(),
          SliverList(delegate: SliverChildListDelegate([
            ...accountItems()
          ]))
        ]
      )
    );
  }

  List<Widget> accountItems(){
    List<Bln> accounts = Provider.of<AccountProvider>(context).getAccounts();
    if (accounts.length > 0) {
      return accounts.map((bln)=>ListTile(
        title: Text(bln.name),
        subtitle: Text(shortAddress(bln.address, 15)),
        leading: new CircleAvatar(
          backgroundImage: AssetImage('assets/images/1024.png'),
        ),
        trailing: new Icon(Icons.navigate_next),
        onTap: () async{
          OkNavigator.push(context, "${AccountRouter.accountUnlock}?address=${bln.address}&name=${bln.name}");
        },
      )).toList();
    }
    return [ListTile(
      title: Center(
        child: Text(S.of(context).noAccounts),
      ),
      // subtitle: Center(
      //   child: Text("you can create a new account."),
      // ),
    )];
  }

  // Widget get _accountButton => Container(
  //   margin: ThemeLayout.top(20),
  //   child: Column(
  //     mainAxisAlignment: MainAxisAlignment.center,
  //     children: [
  //       RaisedButton(
  //         child: Text(S.of(context).craeteWallet),
  //         onPressed: () {
  //           OkNavigator.push(context, AccountRouter.accountCreatePage1);
  //         }
  //       ),
  //       FlatButton(
  //         child: Text(S.of(context).importWallet),
  //         onPressed: () {
  //           showImportButtonMenu(context);
  //         }
  //       )
  //     ],
  //   )
  // );

}
