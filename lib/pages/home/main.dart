import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:blacknet/pages/home/provider/main.dart';
import 'package:blacknet/provider/global.dart';
import 'package:blacknet/pages/wallet/index.dart';
import 'package:blacknet/pages/me/index.dart';
import 'package:blacknet/pages/pos/index.dart';
import 'package:blacknet/generated/i18n.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPage createState() => _MainPage();
}


class _MainPage extends State<MainPage> with WidgetsBindingObserver {

  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();
  final MainProvider provider = MainProvider(); 
  final _pageController = PageController();

  List<Widget> _pageList;

  @override
  void initState() {
    super.initState();
    // 添加切换监听
    WidgetsBinding.instance.addObserver(this);
    // 初始化页面
    initNavPages();
  }
  
  @override
  Widget build(BuildContext context) {
    
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: provider)
      ],
      child: Scaffold(
        bottomNavigationBar: Consumer<MainProvider>(
          builder: (_, provider, __){
            return BottomNavigationBar(
              currentIndex: provider.value,
              items: getBottomNavItems(),
              backgroundColor: Theme.of(context).bottomAppBarColor,
              onTap: (int index) {
                _pageController.jumpToPage(index);
              },
            );
          },
        ),
        // 使用PageView的原因参看 https://zhuanlan.zhihu.com/p/58582876
        body: PageView(
          controller: _pageController,
          onPageChanged: onPageChanged,
          children: _pageList,
          physics: NeverScrollableScrollPhysics(), // 禁止滑动
        )
      )
    );
  }

  void onPageChanged(int index) {
    provider.value = index;
  }

  void initNavPages() {
    // 初始化页面
    _pageList = [
      WalletPage(),
      PoSPage(),
      MePage()
    ];
  }

  List<BottomNavigationBarItem> getBottomNavItems(){
    return [
        BottomNavigationBarItem(
          icon: Icon(Icons.account_balance_wallet),
          title: Text(S.of(context).wallet)
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.payment),
          title: Text(S.of(context).pos)
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person_outline),
          title: Text(S.of(context).me)
        ),
      ];
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("--" + state.toString());
    switch (state) {
      case AppLifecycleState.inactive: // 处于这种状态的应用程序应该假设它们可能在任何时候暂停。
        Provider.of<GlobalProvider>(context).setInActive(DateTime.now().millisecondsSinceEpoch);
        break;
      case AppLifecycleState.resumed:// 应用程序可见，前台
        int lastTime = Provider.of<GlobalProvider>(context).getInActive();

        if((DateTime.now().millisecondsSinceEpoch - lastTime) > 1 ){

          Provider.of<GlobalProvider>(context).setPassCodeAuth(true);
        }
        break;
      case AppLifecycleState.paused: // 应用程序不可见，后台
        Provider.of<GlobalProvider>(context).setInActive(DateTime.now().millisecondsSinceEpoch);
        break;
      case AppLifecycleState.detached: // 申请将暂时暂停
        // break;
      // case AppLifecycleState.suspending:
        // TODO: Handle this case.
        break;
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _verificationNotifier.close();
    super.dispose();
  }
}
