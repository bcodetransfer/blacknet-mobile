import 'dart:async';
import 'package:blacknet/components/progress_dialog/progress_dialog.dart';
import 'package:blacknet/services/LocalAuthenticationService.dart';
import 'package:blacknet/services/service_locator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// provider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/tx.dart';
// router
import 'package:blacknet/routers/routers.dart';
// theme
import 'package:package_info/package_info.dart';

// widgets
import 'package:blacknet/widgets/index.dart';
// res
import 'package:blacknet/res/index.dart';
// utils
import 'package:blacknet/utils/index.dart';
// models
import 'package:blacknet/models/index.dart';
// apis
import 'package:blacknet/components/api/api.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';
// constants
import 'package:blacknet/constants/index.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPage createState() => _AboutPage();
}

class _AboutPage extends State<AboutPage>
    with
        AutomaticKeepAliveClientMixin<AboutPage>,
        SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => false;

  final TextEditingController _amountController = TextEditingController();
  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();

  final LocalAuthenticationService _localAuth =
      locator<LocalAuthenticationService>();

  final _formKey = GlobalKey<FormState>();

  double fee = 0.001;
  double amount = 1000;
  String to = Constants.donateAddress;
  String msg = Constants.donateMessage;
  bool encrypted = false;

  @override
  void initState() {
    super.initState();
  }

  Future<void> _forSubmitted(BuildContext context1) async {
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    BlnTxns _tx = BlnTxns();

    bool checkResult = await _localAuth.auth('Submitting');
    if (checkResult) {
      //tx
      _tx.data = BlnTxnsData();
      _tx.data.amount = (amount * 1e8).toDouble();
      _tx.from = bln.address;
      _tx.data.to = to;
      _tx.fee = (fee * 1e8).toInt();
      _tx.type = 0;
      _tx.time =
          (DateTime.now().microsecondsSinceEpoch / 1e6).toInt().toString();
      var mnemonic = await bln.getMnemonic();
      var res = await Api.transfer(mnemonic, fee, amount, to, encrypted, msg);
      dismissProgressDialog();
      if (res.code == 200) {
        //balance
        BlnBalance balance = Provider.of<AccountProvider>(context).getBalance();
        balance.balance -= _tx.data.amount.toInt();
        // balance notify
        Provider.of<AccountProvider>(context).setBalance(balance);
        // eventBus.fire(TxListRefreshEvent(_tx));
        Provider.of<TxProvider>(context).addTxlistAsync([_tx], _tx.type);
        // 跳转页面
        Navigator.pushNamed(context, WalletRouter.walletTransactionDetail,
            arguments: TxDetailPageArguments(bln: _tx));
      } else {
        dismissProgressDialog();
      }
    }else{
      dismissProgressDialog();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(S.of(context).about), centerTitle: true),
        body: ListView(children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 40, bottom: 30),
            child: Align(
              alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  Container(
                    child: new Image(
                        image: new AssetImage('assets/images/logo.png')),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child:
                        Text("Blacknet Mobile", style: TextStyle(fontSize: 18)),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: FutureBuilder(
                      future: getVersionNumber(),
                      builder: (BuildContext context,
                              AsyncSnapshot<String> snapshot) =>
                          Text(snapshot.hasData ? snapshot.data : " ..."),
                    ),
                  )
                ],
              ),
            ),
          ),
          ListTile(
            title:
                Text(Constants.gitlab, style: TextStyle(color: Colours.violet)),
            onTap: () {
              OkNavigator.goWebViewPage(
                  context, "Gitlab.com", Constants.gitlab);
            },
          )
        ]));
  }

  Future<String> getVersionNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = packageInfo.version;

    return version;
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    // pass
    _verificationNotifier.close();
    _amountController.dispose();
    super.dispose();
  }
}
