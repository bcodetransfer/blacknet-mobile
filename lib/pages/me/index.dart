import 'dart:async';
import 'dart:ui' as ui;

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';

import 'package:blacknet/components/index.dart';
import 'package:blacknet/services/LocalAuthenticationService.dart';
import 'package:blacknet/services/service_locator.dart';
import 'package:blacknet/utils/index.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/global.dart';
import 'package:blacknet/models/index.dart';
import 'package:blacknet/routers/routers.dart';
import 'package:blacknet/generated/i18n.dart';
import 'package:blacknet/utils/sp.dart';

class MePage extends StatefulWidget {
  @override
  _MePage createState() => _MePage();
}

class _MePage extends State<MePage>
    with AutomaticKeepAliveClientMixin<MePage>, SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => false;

  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();

  final LocalAuthenticationService _localAuth = locator<LocalAuthenticationService>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _verificationNotifier.close();
  }

  Widget i18n(languageModel) {
    return new ListTile(
      title: Text(S.of(context).language),
      leading: new CircleAvatar(
          child: new Icon(Icons.language),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(S.of(context).lang(languageModel.titleId)),
          Icon(Icons.keyboard_arrow_right)
        ],
      ),
      onTap: () {
        OkNavigator.push(context, Routes.language);
      },
    );
  }

  Widget get about {
    return new ListTile(
      title: Text(S.of(context).about),
      leading: new CircleAvatar(
          child: new Text("Ab"),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      trailing: FutureBuilder(
        future: getVersionNumber(),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) =>
            Text(snapshot.hasData ? snapshot.data : " ..."),
      ),
      onTap: () {
        OkNavigator.push(context, Routes.about);
      },
    );
  }

  void logoutAction() async {
    var close = await showDialog<bool>(
      context: context,
      barrierDismissible: true, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).logoutTitle),
          actions: <Widget>[
            FlatButton(
              child: Text(S.of(context).cancel),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            FlatButton(
              child: Text(S.of(context).logout),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            )
          ],
        );
      },
    );
    if (close != null && close) {
      Provider.of<AccountProvider>(context).setCancelLease([]);
      Provider.of<AccountProvider>(context).setBalance(BlnBalance.empty());
      Provider.of<AccountProvider>(context).setCurrentBln(Bln.empty());
      OkNavigator.push(context, Routes.index, replace: true, clearStack: true);
    }
  }

  Widget get logout {
    return ListTile(
      title: Text(S.of(context).logout),
      leading: new CircleAvatar(
          child: new Icon(Icons.exit_to_app),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      onTap: logoutAction,
      enabled: true,
    );
  }

  Future<void> checkPasswordAndShowCode(context) async{
    
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    bool checkResult = await _localAuth.auth('Checking');
    dismissProgressDialog();
    if(checkResult){
      var mnemonic = await bln.getMnemonic();
      
      OkNavigator.push(context, '${Routes.accountBackup}?address=${bln.address}&mnemonic=${mnemonic}');
    }
  }

  Widget backupMnemonicCode(context) {
    return ListTile(
      title: Text(S.of(context).backupMnemonic),
      leading: new CircleAvatar(
          child: new Icon(Icons.lock),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      onTap: () async{
        await checkPasswordAndShowCode(context);
      },
      enabled: true,
    );
  }
  
  Widget paymentManagement(context) {
    return ListTile(
      title: Text(S.of(context).paymentManagement),
      leading: new CircleAvatar(
          child: new Icon(Icons.lock),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      onTap: () {

      },
      enabled: true,
    );
  }

  @override
  Widget build(BuildContext context) {

    Locale l = Provider.of<GlobalProvider>(context).getLanguage();
    LanguageModel languageModel = LanguageModel(S.delegate.title(l),
        l.languageCode, l.countryCode, S.delegate.getLiteralText(l));
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    if (bln.isEmpty()) {
      return Scaffold();
    }
    return Scaffold(
      appBar: AppBar(title: Text('Account'), centerTitle: true, elevation: 0),
        body: ListView(
      padding: EdgeInsets.zero,
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        ListTile(
            contentPadding: EdgeInsets.only(top: 20, bottom: 15, left: 20, right: 20),
            title: Text(bln.address,
                style: TextStyle(
                    color:
                        Theme.of(context).appBarTheme.textTheme.title.color)),
            trailing: new Icon(Icons.content_copy, size: 16),
            onTap: () {
              Clipboard.setData(new ClipboardData(text: bln.address)).then((v) {
                Toast.show(S.of(context).copySuccess);
              });
            },
          ),
        Divider(),
        backupMnemonicCode(context),
        i18n(languageModel),
        about,
        logout
      ],
    ));
  }

  Future<ui.Image> _loadOverlayImage() async {
    final completer = Completer<ui.Image>();
    final byteData = await rootBundle.load('assets/images/logo.png');
    ui.decodeImageFromList(byteData.buffer.asUint8List(), completer.complete);
    return completer.future;
  }

  Future<String> getVersionNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = packageInfo.version;

    return version;
  }
}
