
import 'package:fluro/fluro.dart';

import 'package:blacknet/routers/router_init.dart';

// wallet pages
import 'transfer_page.dart';
import 'transaction_page.dart';
import 'lease_page.dart';
import 'cancellease_page.dart';
import 'signMessage_page.dart';
import 'verifyMessage_page.dart';
import 'txdetail_page.dart';
import 'index.dart';

class WalletRouter implements IRouterProvider{
  
  static String walletTransfer = "/wallet/transfer";
  static String walletLease = "/wallet/lease";
  static String walletTransaction = "/wallet/transaction";
  static String walletTransactionDetail = "/wallet/transaction/detail";
  static String walletCancelLease = "/wallet/cancellease";
  static String walletSignMessage = "/wallet/signmessage";
  static String walletVerifyMessage = "/wallet/verifymessage";
  
  @override
  void initRouter(Router router) {
    // wallet
    router.define(walletTransfer, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new TransferPage(
        fee: params['fee']?.first,
        amount: params['amount']?.first,
        to: params['to']?.first,
        msg: params['msg']?.first,
        encrypted: params['encrypted']?.first
      );
    }));
    router.define(walletTransaction, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new TransactionPage();
    }));
    router.define(walletLease, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new LeasePage();
    }));
    router.define(walletCancelLease, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new CancelLeasePage(
        address: params['address']?.first
      );
    }));
    router.define(walletSignMessage, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new SignMessagePage();
    }));
    router.define(walletVerifyMessage, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new VerifyMessagePage();
    }));
    router.define(walletTransactionDetail, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return TxDetailPage();
    }));
    
  }

}