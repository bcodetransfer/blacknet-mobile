import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:blacknet/provider/datacenter.dart';
import 'package:blacknet/pages/wallet/provider/list.dart';
import 'package:blacknet/res/index.dart';
import 'package:blacknet/utils/index.dart';
import 'package:blacknet/models/index.dart';
import 'package:blacknet/generated/i18n.dart';
import 'package:blacknet/widgets/index.dart';
import 'package:blacknet/routers/routers.dart';
import 'package:blacknet/db/txns.dart';

import 'router.dart';

class TransactionList extends StatefulWidget {

  const TransactionList({
    Key key,
    @required this.index,
    @required this.address,
    @required this.type,
  }): super(key: key);

  final int index;
  final int type;
  final String address;
  
  @override
  _TransactionListState createState() => _TransactionListState();
}

class _TransactionListState extends State<TransactionList> with AutomaticKeepAliveClientMixin<TransactionList>{

  /// 是否正在加载数据
  bool _isLoading = false;
  int _page = 1;
  int _maxPage = 1;
  int _index = 0;
  int _type = 0;
  String _address;
  StateType _stateType = StateType.loading;
  ScrollController _controller = ScrollController();
  TxDbHelper _txDb = TxDbHelper();
  
  @override
  void initState() {
    super.initState();
    _index = widget.index;
    _type  = widget.type;
    _address = widget.address;
  }


  @override
  Widget build(BuildContext context) {
    String emptyStr = S.of(context).noTransaction;
    switch (_type) {
      case 2:
        emptyStr = S.of(context).noOutLeases;
        break;
      case 254:
        emptyStr = S.of(context).noPosGenerated;
        break;
      default:
        emptyStr = S.of(context).noTransaction;
    }

    return NotificationListener(
      onNotification: (ScrollNotification note){
        if(note.metrics.pixels == note.metrics.maxScrollExtent){
          _loadMore();
        }
        return true;
      },
      child: RefreshIndicator(
        onRefresh: _onRefresh,
        displacement: 100.0, /// 默认40， 多添加的80为Header高度
        child: Consumer<TransactionListProvider>(
          builder: (_, provider, child) {
            return CustomScrollView(
              /// 这里指定controller可以与外层NestedScrollView的滚动分离，避免一处滑动，5个Tab中的列表同步滑动。
              /// 这种方法的缺点是会重新layout列表
              controller: _index != provider.index ? _controller : null,
              key: PageStorageKey<String>("$_index"),
              slivers: <Widget>[
                SliverOverlapInjector(
                  ///SliverAppBar的expandedHeight高度,避免重叠
                  handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                ),
                child
              ],
            );
          },
          child: Consumer<DataCenterProvider>(
            builder: (_, provider, __){
              List<BlnTxns> list = provider.getTxns(_type);
              return SliverPadding(
                padding: const EdgeInsets.symmetric(horizontal: 0.0),
                sliver: list == null || list.isEmpty ? SliverFillRemaining(child: list != null && list.isEmpty ? StateLayout.empty(emptyStr):StateLayout(type: _stateType)) :
                SliverList(
                  delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                    return index < list.length ? TransactionListItem(key: ValueKey<String>(list[index].txid), data: list[index], address: _address) : MoreWidget(list.length, _hasMore(), 100);
                  },
                  childCount: list.length + 1
                  ),
                ),
              );
            }
          )
        ),
      ),
    );
  }

  List<BlnTxns> _list;

  Future _onRefresh() async {
    // balnace
    await Provider.of<DataCenterProvider>(context).refershTxnsByType(_type);
  }

  bool _hasMore(){
    return _page < _maxPage;
  }

  Future _loadMore() async {
    if (_isLoading) {
      return;
    }
    if (!_hasMore()){
      return;
    }
    _isLoading = true;
    
  }
  
  @override
  bool get wantKeepAlive => true;
}

class TransactionListItem extends StatefulWidget {
  const TransactionListItem({
    Key key,
    this.data,
    this.address,
  }): super(key: key);

  final BlnTxns data;
  final String address;

  @override
  _TransactionListItemState createState() => _TransactionListItemState();
}

class _TransactionListItemState extends State<TransactionListItem> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> animation;
  BlnTxns data;
  String address;
  @override
  void initState() {
    super.initState();
    address = widget.address;
    data = widget.data;
    _controller = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );
    animation = Tween<double>(begin: 0, end: 1).animate(_controller)..addListener(() => setState(() {}));
    // _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    
    return _build();
  }

  bool _isReceive(){

    switch(data.type){
      case TxType.Generated: 
      case TxType.CancelLease:return address == data.from;break;
      case TxType.Transfer:  
      case TxType.Lease: return address == data.data.to;break;
    }
    return false;
  }

  String _blnTypeName(){
    if(BlnTxData[data.type] == "transfer"){
      return _isReceive() ? S.of(context).transferReceived: S.of(context).transferSend;
    }
    // lease
    if(BlnTxData[data.type] == "lease"){
      return _isReceive() ? S.of(context).leaseIn: S.of(context).leaseOut;
    }
    return S.of(context).blnType(BlnTxData[data.type]);
  }

  Widget _build(){
    final isReceive = _isReceive();
    return InkWell(
      borderRadius:new BorderRadius.circular(20.0),//给水波纹也设置同样的圆角
      onTap: () {
        Navigator.pushNamed(context, WalletRouter.walletTransactionDetail,
          arguments: TxDetailPageArguments(
            bln: data
          )
        );
      },
      child: new Container(
        margin: EdgeInsets.only(left: 15, right: 15),
        padding: EdgeInsets.only(top: 12, bottom: 12),
        decoration: new BoxDecoration(
          border: Border(bottom: BorderSide(color: Colours.lighter, width: 0.5))
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text(
                _blnTypeName(),
                // style: Theme.of(context).textTheme.display1,
                style: TextStyle(
                  color: Colours.blnType
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  (isReceive ? "+": "-")+ fomartBalance(data.data.amount, 8)+ " BLN",
                  overflow: TextOverflow.ellipsis,
                  // style: Theme.of(context).textTheme.display2,
                  style: TextStyle(
                    color: isReceive?Colours.blnRecevied:Colours.blnSent
                  ),
                )
              ),
            )
          ],
        )
      ),
    );
  }
}