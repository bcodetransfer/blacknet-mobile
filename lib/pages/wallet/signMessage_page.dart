import 'dart:async';
import 'package:blacknet/components/index.dart';
import 'package:blacknet/res/gaps.dart';
import 'package:blacknet/services/LocalAuthenticationService.dart';
import 'package:blacknet/services/service_locator.dart';
import 'package:blacknet/utils/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:blacknet/widgets/index.dart';
import 'package:blacknet/generated/i18n.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/models/index.dart';

import 'package:blacknet_lib/blacknet.dart';

class SignMessagePage extends StatefulWidget {
  @override
  _SignMessagePage createState() => _SignMessagePage();
}

class _SignMessagePage extends State<SignMessagePage>
    with
        AutomaticKeepAliveClientMixin<SignMessagePage>,
        SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => false;

  final TextEditingController _amountController = TextEditingController();
  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();
  final LocalAuthenticationService _localAuth = locator<LocalAuthenticationService>();

  final _formKey = GlobalKey<FormState>();
  var message = '';
  var hexSign = '';
  @override
  void initState() {
    super.initState();
  }

  Widget get textArea {
    return TextFormField(
        autofocus: false,
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(10),
            border: InputBorder.none,
            focusedBorder: InputBorder.none),
        maxLines: 3,
        autovalidate: true,
        onChanged: (val) {
          message = val;
        },
        validator: (value) {
          return null;
        });
  }

  Widget get textAreaTitle {
    return Container(
        color: Theme.of(context).canvasColor,
        child: ListTile(
            title: Container(
                padding: const EdgeInsets.only(top: 1),
                child: Text(S.of(context).signMessageTitle))));
  }

  void sign() async {

    var start  = new DateTime.now().millisecondsSinceEpoch;
    
    bool checkResult = await _localAuth.auth('Signing');
    setState(() {
      hexSign = '';
    });
    
    if(checkResult){
      
      Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
      var mnemonic = await bln.getMnemonic();
      var time  = new DateTime.now().millisecondsSinceEpoch;
      print(time - start);
      var hex = Blacknet.sign(mnemonic, message);
      var end = new DateTime.now().millisecondsSinceEpoch;
      dismissProgressDialog();
      print(end - time);
      setState(() {
        hexSign = hex;
      });
    }else{
      dismissProgressDialog();
    }
  }

  Widget get signButton {
    return Container(
        margin: EdgeInsets.only(top: 40),
        padding: EdgeInsets.only(left: 40, right: 40),
        child: MyButton(
            key: Key("Sign"), text: S.of(context).sign, onPressed: sign));
  }

  Widget get pageForm {
    return Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Card(
              margin: const EdgeInsets.only(top: 25),
              elevation: 3,
              child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                textAreaTitle,
                textArea,
              ]),
            )
          ],
        ));
  }

  Widget get resultPanel {
    return Card(
      margin: const EdgeInsets.only(top: 25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            trailing: Icon(Icons.content_copy,
                color: Theme.of(context).accentColor, size: 16),
            title: Container(
              padding: const EdgeInsets.only(top: 10),
              child: Text("Signature"),
            ),
            subtitle: Container(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: Text(hexSign),
            ),
            onTap: () {
              Clipboard.setData(new ClipboardData(text: hexSign)).then((v) {
                Toast.show(S.of(context).copySuccess);
              });
            },
          )
        ],
      ),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
            AppBar(
              title: Text(S.of(context).signMessage), 
              centerTitle: true,
              elevation: 0,
              backgroundColor: Theme.of(context).canvasColor
            ),
        body: MyPage(
            child: ListView(children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: pageForm),
          signButton,
          hexSign.length > 0 ? resultPanel : Gaps.empty
        ])));
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    // pass
    _verificationNotifier.close();
    _amountController.dispose();
    super.dispose();
  }
}
