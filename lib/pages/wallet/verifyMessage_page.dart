import 'dart:async';
import 'package:blacknet/components/index.dart';
import 'package:blacknet/provider/datacenter.dart';
import 'package:blacknet/res/gaps.dart';
import 'package:blacknet/services/LocalAuthenticationService.dart';
import 'package:blacknet/services/service_locator.dart';
import 'package:blacknet/utils/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:blacknet/widgets/index.dart';
import 'package:blacknet/generated/i18n.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/models/index.dart';

class VerifyMessagePage extends StatefulWidget {
  @override
  _VerifyMessagePage createState() => _VerifyMessagePage();
}

class _VerifyMessagePage extends State<VerifyMessagePage>
    with
        AutomaticKeepAliveClientMixin<VerifyMessagePage>,
        SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => false;

  final TextEditingController _amountController = TextEditingController();
  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();
  final LocalAuthenticationService _localAuth = locator<LocalAuthenticationService>();

  final _formKey = GlobalKey<FormState>();
  var message = '';
  var signature = '';
  var result = false;
  var account = '';
  @override
  void initState() {
    super.initState();
  }



  Widget getTextarea(onchange, [String hintText]){

    return TextFormField(
        autofocus: false,
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(10),
            border: InputBorder.none,
            hintText: hintText,
            focusedBorder: InputBorder.none),
        maxLines: 1,
        autovalidate: true,
        onChanged: onchange,
        validator: (value) {
          return null;
        });
  }

  Widget getTextAreaTitle(title){

    return Container(
        color: Theme.of(context).canvasColor,
        child: ListTile(
            title: Container(
                padding: const EdgeInsets.only(top: 0),
                child: Text(title))));
  }


  void verify() async {

    try{
    
      var res = Provider.of<DataCenterProvider>(context).verifyMessage(signature, message, account);
      
      if(!res){
        
        Toast.show('Verify message failed', duration: 2000);
      }
      setState(() {
        result = res;
      });
    }catch(e){
      Toast.show(e.toString(), duration: 2000);
    }
    
  }

  Widget get signButton {
    return Container(
        margin: EdgeInsets.only(top: 40),
        padding: EdgeInsets.only(left: 40, right: 40),
        child: MyButton(
            key: Key("Verify"), text: S.of(context).verify, onPressed: verify));
  }

  Widget getInputCard(title, onchange, [String hintText]){

    return Card(
              margin: const EdgeInsets.only(top: 10),
              elevation: 3,
              child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                getTextAreaTitle(title),
                getTextarea(onchange, hintText),
              ]),
            );
  }

  Widget get pageForm {
    var defaultAccount = Provider.of<DataCenterProvider>(context).getBalance().address;
    return Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            getInputCard('Message', (val){ 
              message = val;
              setState(() {
                result = false;
              });
            }),
            getInputCard('Signature', (val){
              signature = val;
              setState(() {
                result = false;
              });
            }),
            getInputCard('Account', (val){ account = val;}, defaultAccount)
          ],
        ));
  }

  Widget get resultPanel {
    return Card(
      margin: const EdgeInsets.only(top: 25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            subtitle: Container(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: Text('Verify message result: ' + result.toString()),
            )
          )
        ],
      ),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
            AppBar(
              title: Text(S.of(context).verifyMessage), 
              centerTitle: true,
              elevation: 0,
              backgroundColor: Theme.of(context).canvasColor
            ),
        body: MyPage(
            child: ListView(children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: pageForm),
          signButton,
          result ? resultPanel : Gaps.empty
        ])));
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    // pass
    _verificationNotifier.close();
    _amountController.dispose();
    super.dispose();
  }
}
