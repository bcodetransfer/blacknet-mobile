import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:provider/provider.dart';
import 'package:blacknet/mvp/mvp.dart';
import 'package:blacknet/theme/index.dart';
import 'package:blacknet/provider/datacenter.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/res/index.dart';
import 'package:blacknet/models/index.dart';
import 'package:blacknet/utils/index.dart';
import 'package:blacknet/generated/i18n.dart';
import 'package:blacknet/routers/routers.dart';
import 'package:blacknet/widgets/index.dart';


import 'package:blacknet/pages/wallet/presenter/wallet_presenter.dart';
import 'router.dart';

class WalletPage extends StatefulWidget {
  @override
  WalletPageState createState() => WalletPageState();
}

class WalletPageState extends BasePageState<WalletPage, WalletPagePresenter> with AutomaticKeepAliveClientMixin<WalletPage>, SingleTickerProviderStateMixin{
  
  @override
  WalletPagePresenter createPresenter() {
    return WalletPagePresenter();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: onRefresh,
        displacement: 88.0, /// 默认40， 多添加的80为Header高度
        child: CustomScrollView(
          slivers: <Widget>[
            getSliverAppBar(),
            SliverList(delegate: SliverChildListDelegate([
              getBalance(),
              _sendItem,
              _receiveItem,
              _historyItem,
              _leaseItem,
              cancelLeaseItem(),
              _signMessage,
              _verifyMessage
            ]))
          ],
        ),
      )
    );
  }

  Future onRefresh() async {
    // balnace
    await Provider.of<DataCenterProvider>(context).refershBalance();
  }

  SliverAppBar getSliverAppBar() {
    return SliverAppBar(
      backgroundColor: Theme.of(context).canvasColor,
      leading: Gaps.empty,
      elevation: 0.0,
      pinned: true, // 固定在顶部
      // bottom: getBalance(),
      actions: <Widget>[
        IconButton( // action button
          icon: new Icon(Icons.more_horiz, color: Theme.of(context).accentColor),
          onPressed: () { },
        )
      ],
    );
  }

  Widget getBalance() {
    // DataCenterProvider dprovider = Provider.of<DataCenterProvider>(context);
    // double currentBalance = dprovider.getBalanceWithOffset();
    double currentBalance = 1.0;
    String text = fomartBalance(currentBalance) + ' BLN';
    return PreferredSize(
      preferredSize: const Size.fromHeight(150),
      child: Container(
        height: 150,
        child: Align(
          child: Text(text, 
            style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: 24
            )
          )
        )
      )
    );
  }

  Widget get _sendItem => ListTile(
    title: Text(S.of(context).send,
      style: TextStyle(
        color: Theme.of(context).accentColor
      ),
    ),
    leading: new CircleAvatar(
        child: new Icon(Icons.call_made),
        foregroundColor: Theme.of(context).accentColor,
        backgroundColor: Theme.of(context).backgroundColor),
    trailing: new Icon(Icons.navigate_next, color: Theme.of(context).accentColor),
    onTap: () {
      OkNavigator.push(context, WalletRouter.walletTransfer);
    },
  );

  Widget get _receiveItem => ListTile(
    title: Text(S.of(context).transferReceived,
      style: TextStyle(
        color: Theme.of(context).accentColor
      ),
    ),
    leading: new CircleAvatar(
        child: new Icon(Icons.call_received),
        foregroundColor: Theme.of(context).accentColor,
        backgroundColor: Theme.of(context).backgroundColor),
    trailing: new Icon(Icons.navigate_next, color: Theme.of(context).accentColor),
    onTap: () async {
      await showQRcode(); 
    },
  );

  Widget get _historyItem => ListTile(
    title: Text(S.of(context).transactionHistory,
      style: TextStyle(
        color: Theme.of(context).accentColor
      ),
    ),
    leading: new CircleAvatar(
        child: new Icon(Icons.list),
        foregroundColor: Theme.of(context).accentColor,
        backgroundColor: Theme.of(context).backgroundColor),
    trailing: new Icon(Icons.navigate_next, color: Theme.of(context).accentColor),
    onTap: () {
      OkNavigator.push(context, WalletRouter.walletTransaction);
    },
  );

  Widget get _leaseItem => ListTile(
    title: Text(S.of(context).lease,
      style: TextStyle(
        color: Theme.of(context).accentColor
      ),
    ),
    leading: new CircleAvatar(
        child: new Icon(Icons.arrow_forward),
        foregroundColor: Theme.of(context).accentColor,
        backgroundColor: Theme.of(context).backgroundColor),
    trailing: new Icon(Icons.navigate_next, color: Theme.of(context).accentColor),
    onTap: () {
      OkNavigator.push(context, WalletRouter.walletLease);
    },
  );

  Widget cancelLeaseItem(){
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    return ListTile(
      title: Text(S.of(context).cancelLease,
        style: TextStyle(
          color: Theme.of(context).accentColor
        ),
      ),
      leading: new CircleAvatar(
          child: new Icon(Icons.arrow_back),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).backgroundColor),
      trailing: new Icon(Icons.navigate_next, color: Theme.of(context).accentColor),
      onTap: () {
        OkNavigator.push(context, '${WalletRouter.walletCancelLease}?address=${bln.address}');
      },
    );
  }

  Widget get _signMessage => ListTile(
    title: Text(S.of(context).signMessage,
      style: TextStyle(
        color: Theme.of(context).accentColor
      ),
    ),
    leading: new CircleAvatar(
        child: new Icon(Icons.create),
        foregroundColor: Theme.of(context).accentColor,
        backgroundColor: Theme.of(context).backgroundColor),
    trailing: new Icon(Icons.navigate_next, color: Theme.of(context).accentColor),
    onTap: () {
      OkNavigator.push(context, WalletRouter.walletSignMessage);
    },
  );

  Widget get _verifyMessage => ListTile(
    title: Text(S.of(context).verifyMessage,
      style: TextStyle(
        color: Theme.of(context).accentColor
      ),
    ),
    leading: new CircleAvatar(
        child: new Icon(Icons.message),
        foregroundColor: Theme.of(context).accentColor,
        backgroundColor: Theme.of(context).backgroundColor),
    trailing: new Icon(Icons.navigate_next, color: Theme.of(context).accentColor),
    onTap: () {
      OkNavigator.push(context, WalletRouter.walletVerifyMessage);
    },
  );

  showQRcode() async {
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    String _text = 'blacknet:' + bln.address;
    RoundedRectangleBorder _defaultDialogShape = RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(5.0)));
    EdgeInsetsGeometry _contentPadding = const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0);
    switch (await showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          contentPadding: _contentPadding,
          shape: _defaultDialogShape,
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Container(
                width: ThemeLayout.height(280),
                height: ThemeLayout.height(280),
                child: QrcodeHelper.generateQRcode(_text),
              ),
            ),
            SizedBox(height: 10),
            ListTile(
              title: Text(S.of(context).address),
              subtitle: Text(shortAddress(bln.address, 28))
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.only(left: 15, right: 15),
              child: MyButton(
                text: S.of(context).copyAddress, 
                onPressed: (){
                  Navigator.pop(context, true);
                })
            )
          ],
        );
      }
    )) {
      case true:
        Clipboard.setData(new ClipboardData(text: bln.address)).then((v) {
          Toast.show(S.of(context).copySuccess);
        });
      break;
    }
  }

}