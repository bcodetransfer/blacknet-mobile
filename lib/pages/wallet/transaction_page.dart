import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/pages/wallet/provider/list.dart';
import 'package:blacknet/pages/wallet/transaction_list.dart';
import 'package:blacknet/theme/index.dart';
import 'package:blacknet/models/index.dart';
import 'package:blacknet/generated/i18n.dart';

class TransactionPage extends StatefulWidget {
  @override
  _TransactionPage createState() => _TransactionPage();
}

class _TransactionPage extends State<TransactionPage> with AutomaticKeepAliveClientMixin<TransactionPage>, SingleTickerProviderStateMixin{

  @override
  bool get wantKeepAlive => true;

  TabController _tabController;
  TransactionListProvider provider = TransactionListProvider();

  List<TransactionPageTab> tabs = <TransactionPageTab>[
      TransactionPageTab(key: BlnTxType.all, title: 'all', icon: Icons.directions_car),
      TransactionPageTab(key: BlnTxType.transfer, title: 'transfer', icon: Icons.directions_bike),
      TransactionPageTab(key: BlnTxType.lease, title: 'lease', icon: Icons.directions_boat),
      TransactionPageTab(key: BlnTxType.posGenerated, title: 'pos', icon: Icons.directions_bus),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: tabs.length);
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    return Scaffold(
      body: ChangeNotifierProvider<TransactionListProvider>(
        create: (_) => provider,
        child: NestedScrollView(
          key: const Key('wallet_list'),
          physics: ClampingScrollPhysics(),
          headerSliverBuilder: (context, boxIsScrolled) {
            return _sliverBuilder(context);
          },
          body: PageView.builder(
              key: const Key('pageView'),
              itemCount: tabs.length,
              onPageChanged: _onPageChange,
              controller: _pageController,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (_, index) {
                return TransactionList(index: index, type: tabs[index].key, address: bln.address);
              }
          )
        )
      )
    );
  }

  List<Widget> _sliverBuilder(BuildContext context) {
    return <Widget>[
      SliverOverlapAbsorber(
        handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
        child: SliverAppBar(
          elevation: 0.0,
          centerTitle: true,
          title: Text(S.of(context).transactionHistory),
          floating: false, // 不随着滑动隐藏标题
          pinned: true, // 固定在顶部
        ),
      ),
      SliverPersistentHeader(
        pinned: true,
        delegate: _SliverPersistentHeaderDelegate(
          TabBar(
            tabs: tabs.map((TransactionPageTab tab) {
              return new Tab(
                text: S.of(context).blnType(tab.title)
              );
            }).toList(),
            controller: _tabController,
            //配置控制器
            isScrollable: false,
            // indicatorColor: Theme.of(context).canvasColor,
            indicatorWeight: 2,
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorPadding: EdgeInsets.only(bottom: 0),
            labelPadding: EdgeInsets.only(left: 0),
            // labelColor: Theme.of(context).backgroundColor,
            labelStyle: TextStyle(
              fontSize: ThemeLayout.fontSize(15),
            ),
            // unselectedLabelColor: Theme.of(context).backgroundColor,
            unselectedLabelStyle: TextStyle(
              fontSize: ThemeLayout.fontSize(15),
            ),
            onTap: (index){
              if (!mounted){
                return;
              }
              _pageController.jumpToPage(index);
            },
          )
        ),
      )
    ];
  }

  PageController _pageController = PageController(initialPage: 0);
  _onPageChange(int index) async{
    provider.setIndex(index);
    _tabController.animateTo(index);
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}

class _SliverPersistentHeaderDelegate extends SliverPersistentHeaderDelegate {

  final TabBar _tabBar;
  
  _SliverPersistentHeaderDelegate(this._tabBar);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Material(
      color: Theme.of(context).primaryColor,
      child: _tabBar,
    );
  }

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
  
}

class TransactionPageTab {
  TransactionPageTab({ this.title, this.icon , this.key});
  String title;
  int key;
  IconData icon;
}