import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';
import 'package:blacknet/mvp/mvp.dart';
import 'package:blacknet/pages/wallet/index.dart';
import 'package:blacknet/net/net.dart';

class WalletPagePresenter extends BasePagePresenter<WalletPageState> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async{
      Bln bln = Provider.of<AccountProvider>(view.context).getCurrentBln();
      String url = Resources.getTxApi() + "/api/account/ledger/" + bln.address;
      // request balance
      asyncRequestNetwork(Method.GET,
        url: url,
        onSuccess: (data) {
          print(data);
          // view.setUser(data);
          // or
          // view.provider.setUser(data);
        },
      );
    });

  }
}