import 'package:flutter/material.dart';

class TransactionListProvider with ChangeNotifier {
    /// Tab的下标
    int _index = 0;
    int get index => _index;
    void refresh(){
      notifyListeners();
    }
    void setIndex(int index) {
      _index = index;
      notifyListeners();
    }

    // double _balance = 0.0;
    // double get balance => _balance;
    // void refreshBalance(){
    //   notifyListeners();
    // }
    // void setBalance(double b) {
    //   _balance = b;
    //   notifyListeners();
    // }

    // cache
    // void _setCache(List<BlnLease> list){
    //   SpUtil.putStringList("transfer_cancellease", list.map((i) => jsonEncode(i)).toList());
    // }

    // List<BlnLease> _getCache(){
    //   var txStrs = SpUtil.getStringList("transfer_cancellease");
    //   return txStrs.map((i)=> BlnLease.fromJson(jsonDecode(i))).toList();
    // }
}