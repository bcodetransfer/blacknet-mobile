
import 'package:blacknet/provider/datacenter.dart';
import 'package:blacknet/res/txtype.dart';
import 'package:blacknet/services/LocalAuthenticationService.dart';
import 'package:blacknet/services/service_locator.dart';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// provider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/tx.dart';
// router
import 'package:blacknet/routers/routers.dart';
// theme
// widgets
import 'package:blacknet/widgets/index.dart';
// res
import 'package:blacknet/res/index.dart';
// utils
import 'package:blacknet/utils/index.dart';
// models
import 'package:blacknet/models/index.dart';
// apis
// i18n
import 'package:blacknet/generated/i18n.dart';
// event

import 'router.dart';

class LeasePage extends StatefulWidget {
  @override
  _LeasePage createState() => _LeasePage();
}

class _LeasePage extends State<LeasePage>
    with
        AutomaticKeepAliveClientMixin<LeasePage>,
        SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => false;

  final TextEditingController _amountController = TextEditingController();
  final TextEditingController _toController = TextEditingController();
  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();
  DataCenterProvider dProvider;

  final LocalAuthenticationService _localAuth =
      locator<LocalAuthenticationService>();
  final _formKey = GlobalKey<FormState>();

  double fee = 0.001;
  double amount;
  String to;
  String msg;
  bool encrypted = false;
  @override
  void initState() {
    super.initState();
  }

  Future<void> scanQR() async {
    String barcodeScanRes = await QrcodeHelper.scanQrAddress();
    if (barcodeScanRes != null && mounted) {
      _toController.text = barcodeScanRes;
      setState(() {
        _toController.text = barcodeScanRes;
        to = barcodeScanRes;
      });
    }
  }

  bool _validate() {
    return to != null &&
        amount != null &&
        _formKey.currentState.validate() &&
        validatorAddress(to) &&
        validatorBalance(amount.toString());
  }

  _reset() {
    fee = 0.001;
    amount = null;
    to = null;
    msg = null;
    encrypted = false;
  }

  Future<void> _forSubmitted(BuildContext context1) async {
    var _form = _formKey.currentState;
    var dcProvider = Provider.of<DataCenterProvider>(context);
    if (_form.validate()) {
      _form.save();

      Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
      BlnTxns tx = BlnTxns();
      tx.data = BlnTxnsData();
      tx.data.amount = (amount * 1e8).toDouble();
      tx.from = bln.address;
      tx.data.to = to;

      tx.fee = (fee * 1e8).toInt();
      tx.type = TxType.Lease;
      tx.time = (DateTime.now().microsecondsSinceEpoch ~/ 1e6).toString();

      String res = await dcProvider.leaseCoins(context, amount, to);

      if (res != 'false') {
        tx.txid = res;
        
        dcProvider.addTxToTemp(tx);
        _form.reset();
        _amountController.text = '';
        _toController.text = '';
        setState(() {
          _reset();
        });
        dcProvider.refershTxnsByType(null);
        dcProvider.refershTxnsByType(tx.type);

        Navigator.pushNamed(context, WalletRouter.walletTransactionDetail,
            arguments: TxDetailPageArguments(bln: tx));
      }
    }
  }

  Widget textInput(
      keyStr, controller, onSaved, onChanged, validator, placeholder) {
    var theme = Theme.of(context).textTheme;
    return TextFormField(
      key: Key(keyStr),
      autocorrect: true,
      controller: controller,
      autofocus: false,
      onSaved: onSaved,
      initialValue: null,
      onChanged: onChanged,
      validator: validator,
      autovalidate: true,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      style: TextStyle(
          textBaseline: TextBaseline.alphabetic, color: theme.display4.color),
      decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
          border: InputBorder.none,
          filled: true,
          fillColor: theme.body1.color,
          hintText: placeholder),
    );
  }

  Widget amountTextInput(realAmount) {
    onSave(val) {
      amount = double.parse(val);
    }

    onChanged(value) {
      try {
        amount = double.parse(value);
      } catch (e) {
        amount = -1;
      }
      setState(() {});
    }

    validator(value) {
      if (amount == null) {
        return null;
      }
      if (!validatorBalance(value)) {
        return S.of(context).amountInputCorrect;
      }
      if (value.length > 0 && double.parse(value) > realAmount) {
        return S.of(context).amountInputEnough;
      }
      return null;
    }

    return textInput('amount', _amountController, onSave, onChanged, validator,
        S.of(context).amountInput);
  }

  Widget acccountTextInput() {
    onSave(val) {
      to = val;
    }

    onChanged(value) {
      setState(() {
        to = value;
      });
    }

    validator(value) {
      if (to == null) {
        return null;
      }
      if (!validatorAddress(value)) {
        return S.of(context).addressInputCorrect;
      }
      return null;
    }

    return textInput('to', _toController, onSave, onChanged, validator,
        S.of(context).addressInput);
  }

  Widget titleRow(String title, String displayText) {
    var displayTextNode = Text(displayText,
        style: TextStyle(color: Theme.of(context).textTheme.display3.color));
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(title),
          FlatButton(
              padding: const EdgeInsets.all(0),
              onPressed: () {
                double realAmount = realBalance(dProvider.getBalance().balance.toDouble());
                amount = realAmount;
                setState(() {});
              },
              child: displayTextNode)
        ]);
  }

  Widget row1(realAmount) {
    var currentText = fomartBalance(dProvider.getBalanceWithOffset()) + ' BLN';
    return Container(
        margin: const EdgeInsets.only(bottom: 16),
        child: ListTile(
            title: titleRow(S.of(context).amount, currentText),
            subtitle: Container(child: amountTextInput(realAmount))));
  }

  Widget row2(
    titleText,
    childText,
  ) {
    var listTitle = ListTile(
        title: titleRow(titleText, childText),
        subtitle: Container(
            child: Stack(children: <Widget>[
          acccountTextInput(),
          Positioned(
              right: 0,
              // top: -3,
              child: IconButton(
                padding: EdgeInsets.all(0),
                icon: Icon(Icons.photo_camera, color: Colours.violet),
                onPressed: () => scanQR(),
              ))
        ])));
    return Container(
        margin: const EdgeInsets.only(bottom: 16),
        child: Column(
            mainAxisSize: MainAxisSize.min, children: <Widget>[listTitle]));
  }

  Widget layout(realAmount) {
    return Expanded(
        flex: 1,
        child: SingleChildScrollView(
            key: const Key('lease'),
            // padding: const EdgeInsets.symmetric(vertical: 16.0),
            physics: BouncingScrollPhysics(),
            child:
                Form(key: _formKey, child: widgetList(realAmount))));
  }

  Widget widgetList(realAmount) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      row1(realAmount),
      row2(S.of(context).address, "")
    ]);
  }

  @override
  Widget build(BuildContext context) {
    dProvider = Provider.of<DataCenterProvider>(context);
    double realAmount = realBalance(dProvider.getBalance().balance.toDouble());

    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).transfer),
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor,
      ),
      body: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        layout(realAmount),
        Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 32),
            child: Container(
                margin: const EdgeInsets.only(top: 25),
                child: MyButton(
                    key: Key("Lease"),
                    text: S.of(context).lease,
                    onPressed: !_validate()
                        ? null
                        : () async {
                            await _forSubmitted(context);
                          })))
      ],
    ));
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    // pass
    _verificationNotifier.close();
    _amountController.dispose();
    _toController.dispose();
    super.dispose();
  }
}
