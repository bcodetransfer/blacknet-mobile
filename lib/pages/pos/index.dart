import 'package:blacknet/provider/datacenter.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:provider/provider.dart';
import 'package:blacknet/pages/pos/provider/pos.dart';
// theme
// api
// models
import 'package:blacknet/models/index.dart';
// utils
import 'package:blacknet/utils/index.dart';
// res
import 'package:blacknet/res/index.dart';
// widgets
import 'package:blacknet/widgets/index.dart';

double height = 270;
class PoSPage extends StatefulWidget {
  @override
  _PoSPage createState() => _PoSPage();
}

class _PoSPage extends State<PoSPage>
    with
        AutomaticKeepAliveClientMixin<PoSPage>,
        SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => false;

  PosProvider posProvider = PosProvider();
  StateType _stateType = StateType.loading;
  List<BlnLease> leaseList ;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future _onRefresh() async {
    await Provider.of<DataCenterProvider>(context).refreshPoSState();
  }

  @override
  Widget build(BuildContext context) {
    DataCenterProvider dcProvider = Provider.of<DataCenterProvider>(context);
    return Scaffold(
        appBar:
            AppBar(centerTitle: true, elevation: 0, title: Text("PoS Profit")),
        body: MultiProvider(
            providers: [ChangeNotifierProvider.value(value: dcProvider)],
            child: NestedScrollView(
                key: const Key('pos'),
                physics: ClampingScrollPhysics(),
                headerSliverBuilder: (context, boxIsScrolled) {
                  return _sliverBuilder(context);
                },
                body: RefreshIndicator(
                  onRefresh: _onRefresh,
                  displacement: 40.0,

                  /// 默认40， 多添加的80为Header高度
                  child: Consumer<DataCenterProvider>(builder: (_, provider, child) {
                    return CustomScrollView(
                      key: PageStorageKey<String>("pos_lisview"),
                      slivers: <Widget>[child],
                    );
                  }, child: Consumer<DataCenterProvider>(builder: (_, provider, __) {
                    List<PosListItem> list = provider.getThePoSlist();
                    return SliverPadding(
                      padding: const EdgeInsets.symmetric(horizontal: 0.0),
                      sliver: list == null || list.isEmpty
                          ? SliverFillRemaining(
                              child: list != null && list.isEmpty
                                  ? StateLayout.empty('No PoS payouts')
                                  : StateLayout(type: _stateType))
                          : SliverList(
                              delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                return ListItem(list[index]);
                              }, childCount: list.length),
                            ),
                    );
                  })),
                ))));
  }

  List<Widget> _sliverBuilder(BuildContext context) {
    return <Widget>[
      SliverPersistentHeader(
        pinned: true,
        delegate: SliverCustomHeaderDelegate(PoSHeader()),
      )
    ];
  }
}

class ListItem extends StatelessWidget {
  PosListItem item;
  ListItem(this.item);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: new BorderRadius.circular(20.0), //给水波纹也设置同样的圆角
      onTap: null,
      child: new Container(
          margin: EdgeInsets.only(left: 15, right: 15),
          padding: EdgeInsets.only(top: 12, bottom: 12),
          decoration: new BoxDecoration(
              border: Border(
                  bottom: BorderSide(color: Colours.lighter, width: 0.5))),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Text(
                  // formatDate(DateTime.now()),
                  item.blockHeight.toString(),
                  style: TextStyle(color: Colours.blnType),
                ),
              ),
              Expanded(
                flex: 1,
                child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      "+" + fomartBalance(item.amount, 8) + " BLN",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colours.blnRecevied),
                    )),
              )
            ],
          )),
    );
  }
}

class PoSHeader extends StatefulWidget {

  @override
  _PoSHeader createState() => _PoSHeader();
}

class _PoSHeader extends State<PoSHeader>
    with
        AutomaticKeepAliveClientMixin<PoSHeader>,
        SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => false;
  double stakingBalance;
  double newStakingBalance;
  double newProfitEst;
  DataCenterProvider dataCenterProvider;
  @override
  Widget build(BuildContext context) {
    dataCenterProvider = Provider.of<DataCenterProvider>(context);
    return Container(
      color: Theme.of(context).bottomAppBarColor,
      child: Column(
        children: <Widget>[
          _getBlanace(context),
          _getStakingInfo(context),
          _getStakingSlide(context),
          _getNavHeader(context)
        ],
      ),
    );
  }

  _getBlanace(BuildContext context) {
    var text  = "0.00000000";
    PosWorker posWorker = Provider.of<DataCenterProvider>(context).getPosWorker();
    if(posWorker!= null) text = posWorker.totalReward.toDouble().toString();
    return Container(
      height: 50,
      child: Center(
          child: Text( '+' + text + ' BLN',
              style: TextStyle(color: Colours.blnRecevied, fontSize: 25))),
    );
  }

  _getStakingInfo(BuildContext context) {

    
    var network = dataCenterProvider.getHashrate();
    var stakers = dataCenterProvider.getPosWorker();
    var hashrate = stakers.hashrate, profitEst;
    var networkhash = network.networkhashrate;


    if(hashrate == null) hashrate = 0;

    profitEst = hashrate / networkhash * 1e8 * 1440 * 20 * 30;
    
    Text hashText() {
      return Text(hashrate.toString() + " BLN",
          style: TextStyle(color: Colours.gold));
    }

    Text stakingText() {
      var str = "", balanceStr, color = Colours.blnSent;

      if (newStakingBalance != null) {
        balanceStr = newStakingBalance.toString() + " BLN";
        str = newStakingBalance < 0 ? balanceStr : "+" + balanceStr;
        if (newStakingBalance >= 0) color = Colours.blnRecevied;
      }

      return Text(str, style: TextStyle(color: color));
    }

    return Container(
        height: 120,
        color: Colours.darker,
        padding: EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),
        child: Column(children: <Widget>[
          Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text("Staking Balance"),
              )),
          Expanded(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[hashText(), stakingText()],
              )),
          Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text("Profit Est."),
              )),
          Expanded(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(profitEst.toStringAsFixed(8) + " BLN/month",
                      style: TextStyle(color: Colours.blnRecevied)),
                  getProfitText(newProfitEst)
                ],
              ))
        ]));
  }

  Text getProfitText(newProfitEst) {
    var text = "", color = Colours.blnSent;

    if (newProfitEst != null) {
      var str = newProfitEst.toStringAsFixed(8) + " BLN/month";
      text = newProfitEst < 0 ? str : "+" + str;
      if (newProfitEst >= 0) color = Colours.blnRecevied;
    }

    return Text(text, style: TextStyle(color: color));
  }

  resetProfit(stakers, hashrate, max) {

      var weight = stakers.hashrate == null ? 0 : stakers.hashrate;
      var newEst = (max - weight) / hashrate.networkhashrate;

      newEst = newEst * 1e8 * 1440 * 20 * 30;

      setState(() {
        stakingBalance = max;
        newStakingBalance = max - weight;
        newProfitEst = newEst;
      });
  }

  leaseAllCoinsToStakePool()async{

    var close = await showDialog<bool>(
      context: context,
      barrierDismissible: true, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Lease all coins to PoS pool?'),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            FlatButton(
              child: Text("I'm Sure. Yes!"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            )
          ],
        );
      },
    );
    if (close != null && close) {
      await dataCenterProvider.leaseAllCoinsToStakePool(context);
    }
  }

  _getStakingSlide(BuildContext context) {
    
    var hashrate = dataCenterProvider.getHashrate();
    var stakers = dataCenterProvider.getPosWorker();
    var balance = dataCenterProvider.getBalance();
    double weight = 0;
    var max;
    if ( hashrate != null && stakers.hashrate != null) {
      weight = stakers.hashrate.toDouble();
    }
    if (stakingBalance == null) {
      stakingBalance = weight;
    }
    max = weight + balance.balance / 1e8;

    var canLeaseBalance = balance.confirmedBalance;

    if(canLeaseBalance > 1e8 * 1000){
      height = 270;
      resetProfit(stakers, hashrate, max);
      return Container(
          height: 50,
          child: Column(children: <Widget>[
            Expanded(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(left: 15, right: 15, top: 10),
                  child: MyButton(
                      key: Key("staking"),
                      text: "One Click to PoS",
                      onPressed: leaseAllCoinsToStakePool
                )))
          ]));
    }else{
      height = 220;
      return Gaps.empty;
    }

    
  }

  _getNavHeader(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(left: 15, right: 15),
        height: 50,
        child: Row(
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Align(
                    alignment: Alignment.centerLeft, child: Text("Height"))),
            Expanded(
                flex: 1,
                child: Align(
                    alignment: Alignment.centerRight, child: Text("Reward")))
          ],
        ));
  }
}

class SliverCustomHeaderDelegate extends SliverPersistentHeaderDelegate {
  PoSHeader header;

  SliverCustomHeaderDelegate(this.header);

  @override
  double get minExtent => height;

  @override
  double get maxExtent => height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return this.header;
  }
}
