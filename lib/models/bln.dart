import 'package:blake2b/utils.dart' as butils;

import 'package:bip39/bip39.dart' as bip39;
import 'dart:typed_data';
import 'package:bip_bech32/bip_bech32.dart';
import 'package:blake2b/blake2b.dart';
import 'package:blake2b/blake2b_hash.dart';
import 'package:blake2b/utils.dart';
import 'package:ed25519/ed25519.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/foundation.dart';
import 'package:hex/hex.dart';
import 'package:blacknet_lib/blacknet.dart';

// crypto
import 'package:blacknet/components/crypto/crypto.dart';
import 'dart:async';


Bln _createBln(List<String> args){
  String name = args[0];
  String password = args[1];
  String mnemonic = Blacknet.generateMnemonic();
  String address  = Blacknet.address(mnemonic);
  return Bln.create(name, password, mnemonic, address);
}

Bln _createBlnByMnemonic(String mnemonic){
  String address  = Blacknet.address(mnemonic);
  return Bln.from(mnemonic: mnemonic, address: address);
}

Future<Bln> createBlnByMnemonic(String mnemonic) async{
    return compute(_createBlnByMnemonic, mnemonic);
}

Future<Bln> createBln(String name, String password) async{
    return compute(_createBln, [name, password]);
}

class Bln {
  static bool checkMnemonic(String mnemonic){
      String upHash = Blake2bHash.hashUtf8String2HexString(mnemonic).toUpperCase();
      List<int> isk = HEX.decode(upHash);
      var sk = Utils.int8list2uint8list(Int8List.fromList(isk));
      if((sk[0] & 0xF0) == 0x10){
        return true;
      }
      return false;
  }

  // 生成助记词
  static String generateMnemonic(){
     return Blacknet.generateMnemonic();
  }

  static final String sigma = "Blacknet Signed Message:\n";

  ///
  /// Message Signature
  ///
  static String signMessage(String mnemonic, String message) {
  
    Uint8List msg = Uint8List.fromList((sigma + message).codeUnits);
    Uint8List hashMessage = Blake2bHash.hash(msg, 0, msg.length);

    var upperCaseHash = Blake2bHash.hashUtf8String2HexString(mnemonic).toUpperCase();
    List<int> isk = HEX.decode(upperCaseHash);
    var sk = butils.Utils.int8list2uint8list(Int8List.fromList(isk));
    var pk = Ed25519.publickey(blake2bHashFunc, sk);

    Uint8List sign = Ed25519.signature(blake2bHashFunc, hashMessage, sk, pk);
    String hexSign = HEX.encode(sign).toUpperCase();

    return hexSign;
  }

  static bool verifyMessage(String account, String signature, String message) {
    
    Uint8List msg = Uint8List.fromList((sigma + message).codeUnits);
    Uint8List hashMessage = Blake2bHash.hash(msg, 0, msg.length);

    Bech32Codec bech32codec = Bech32Codec();

    var publicKey = convertBits(bech32codec.decode(account).data, 5, 8, false);

    return Ed25519.checkvalid(blake2bHashFunc, HEX.decode(signature.toLowerCase()), hashMessage, publicKey);
  }

  // 签名
  static String signature(String mnemonic, String serialized){
    String upHash = Blake2bHash.hashUtf8String2HexString(mnemonic).toUpperCase();
    List<int> isk = HEX.decode(upHash);
    var sk = Utils.int8list2uint8list(Int8List.fromList(isk));
    var pk = Ed25519.publickey(blake2bHashFunc, sk);
    // serialized
    List<int> imsg = HEX.decode(serialized);
    var signedMessage = Utils.int8list2uint8list(Int8List.fromList(imsg));
    // print(signedMessage);
    // 3.Calculate the hash of serialized message
    const signatureSize = 64;
    Uint8List hash = Blake2bHash.hash(signedMessage, signatureSize, signedMessage.length - signatureSize);
    // print(HEX.encode(hash));
    Uint8List signature = Ed25519.signature(blake2bHashFunc, hash, sk, pk);
    // print(HEX.encode(signature));
    // 4. fill eth height part of message
    for(int i=0; i < signature.length; i++){
      signedMessage[i] = signature[i];
    }
    HEX.encode(signedMessage).toString();
    // print(HEX.encode(signedMessage));
    return HEX.encode(signedMessage).toString();
  }
  
  String mnemonic;
  String privateKey;
  String publicKey;
  String address;
  String password;
  String identifier;
  String name;
  bool crypto = false;
  bool localAuth = false;

  Bln.mnemonic(this.mnemonic){
    this.address = this.generateAddress();
  }
  Bln.address(this.address);
  Bln.privateKey(this.privateKey);
  Bln.empty(){
    this.mnemonic = null;
    this.address = null;
    this.name = null;
  }
  Bln.from({this.mnemonic, this.address, this.name});
  Bln.create(this.name, this.password, this.mnemonic, this.address);
  Bln(){
    this.mnemonic = Bln.generateMnemonic();
    this.address = this.generateAddress();
  }

  String generateAddress(){
    String hash = Blake2bHash.hashUtf8String2HexString(this.mnemonic);
    Uint8List hashBytes = Utils.int8list2uint8list(Int8List.fromList(HEX.decode(hash)));
    Uint8List pk = Ed25519.publickey(blake2bHashFunc, hashBytes);
    Uint8List cvtBytes = convertBits(pk, 8, 5, true);
    Bech32Codec bech32codec = Bech32Codec();
    return bech32codec.encode(Bech32("blacknet", cvtBytes));
  }

  Bln.fromJson(Map<String, dynamic> json)
    : mnemonic = json['mnemonic'],
      privateKey = json['privateKey'],
      publicKey = json['publicKey'],
      password = json['password'],
      name = json['name'],
      crypto = json['crypto'] as bool,
      localAuth= json['localAuth'] as bool,
      address = json['address'];

  Map<String, dynamic> toJson() => {
        'mnemonic': mnemonic,
        'privateKey': privateKey,
        'publicKey': publicKey,
        'password': password,
        'address': address,
        'name': name,
        'localAuth': localAuth,
        'crypto': crypto
      };

  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"mnemonic\":\"$mnemonic\"");
    sb.write(",\"privateKey\":\"$privateKey\"");
    sb.write(",\"publicKey\":\"$publicKey\"");
    sb.write(",\"password\":\"$password\"");
    sb.write(",\"address\":\"$address\"");
    sb.write(",\"name\":\"$name\"");
    sb.write(",\"localAuth\":\""+ localAuth.toString() +"\"");
    sb.write(",\"crypto\":\""+ crypto.toString() +"\"");
    sb.write('}');
    return sb.toString();
  }
  bool isEmpty(){
    // 判断账户是否存在显示创建账号页面
    if (address == null || address == "") {
      return true;
    }
    return false;
  }

  bool isLocalAuth(){
    return this.localAuth != null && this.localAuth;
  }

  Future<void> saveMnemonic() async{
    await Crypto().putString(this.address, this.mnemonic, this.password);
    this.mnemonic = null;
    this.crypto = true;
    return;
  }

  Future<String> getMnemonic() async{
    if(this.crypto){
      return await Crypto().getString(this.address, this.password);
    }
    return this.mnemonic;
  }
}

// blance
class BlnBalance {
  int seq;
  int balance;
  int confirmedBalance;
  int stakingBalance;
  String address;
  BlnBalance({this.seq, this.balance, this.confirmedBalance, this.stakingBalance, this.address});
  factory BlnBalance.fromJson(Map<String, dynamic> json){
    return new BlnBalance(
      seq: json["seq"],
      balance: json["balance"],
      confirmedBalance: json["confirmedBalance"],
      stakingBalance: json["stakingBalance"],
      address: json["address"]
    );
  }
  factory BlnBalance.empty(){
    return new BlnBalance(
      seq: 0,
      balance: 0,
      confirmedBalance: 0,
      stakingBalance: 0,
      address: null
    );
  }
  Map<String, dynamic> toJson() => {
    'seq': seq,
    'balance': balance,
    'confirmedBalance': confirmedBalance,
    'stakingBalance': stakingBalance,
    'address': address,
  };
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"seq\":\"$seq\"");
    sb.write(",\"balance\":\"$balance\"");
    sb.write(",\"confirmedBalance\":\"$confirmedBalance\"");
    sb.write(",\"stakingBalance\":\"$stakingBalance\"");
    sb.write(",\"address\":\"$address\"");
    sb.write('}');
    return sb.toString();
  }
}

// Txns
class BlnTxns {
  String blockHash;
  BlnTxnsData data;
  int fee;
  String from;
  int seq;
  String signature;
  int size;
  String time;
  int type;
  String txid;
  String hash;
  BlnTxns({this.txid, this.hash, this.blockHash, this.data, this.fee, this.from, this.seq, this.signature, this.size, this.time, this.type});
  factory BlnTxns.fromJson(Map<String, dynamic> json){
    return new BlnTxns(
      blockHash: json["blockHash"],
      data: BlnTxnsData.fromJson(json["data"]),
      fee: json["fee"] is String ? int.parse(json["fee"]) : json["fee"],
      from: json["from"],
      seq: json["seq"],
      signature: json["signature"],
      size: json["size"],
      time: json["time"],
      type: json["type"],
      hash: json["hash"],
      txid: (json["type"] as int) == 254 && (json["txid"] as String) == null ? json["blockHash"] : json["txid"],
    );
  }
  Map<String, dynamic> toJson() => {
    'blockHash': blockHash,
    'data': data,
    'fee': fee,
    'from': from,
    'seq': seq,
    'signature': signature,
    'size': size,
    'time': time,
    'type': type,
    'hash': hash,
    'txid': txid
  };
  Map<String, dynamic> toMap() => {
    'blockHash': blockHash,
    'fee': fee,
    'fromAddress': from,
    'toAddress': data.to,
    'amount': data.amount,
    'message': data.message,
    'blockHeight': data.blockHeight,
    'seq': seq,
    'signature': signature,
    'size': size,
    'time': time,
    'type': type,
    'txid': txid
  };
  factory BlnTxns.fromMap(Map<String, dynamic> json){
    return new BlnTxns(
      blockHash: json["blockHash"],
      data: BlnTxnsData(blockHeight: json["blockHeight"], to: json["toAddress"], amount: json["amount"], message: json["message"]),
      fee: json["fee"] is String ? int.parse(json["fee"]) : json["fee"],
      from: json["fromAddress"],
      seq: json["seq"],
      signature: json["signature"],
      size: json["size"],
      time: json["time"],
      type: json["type"],
      hash: json["hash"],
      txid: json["txid"],
    );
  }
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"blockHash\":\"$blockHash\"");
    sb.write(",\"data\":\""+ data.toString() +"\"");
    sb.write(",\"fee\":\"$fee\"");
    sb.write(",\"from\":\"$from\"");
    sb.write(",\"seq\":\"$seq\"");
    sb.write(",\"signature\":\"$signature\"");
    sb.write(",\"size\":\"$size\"");
    sb.write(",\"time\":\"$time\"");
    sb.write(",\"type\":\"$type\"");
    sb.write(",\"hash\":\"$hash\"");
    sb.write(",\"txid\":\"$txid\"");
    sb.write('}');
    return sb.toString();
  }
}

// TxnsData
class BlnTxnsData {
  String to;
  double amount;
  int blockHeight;
  String message;
  BlnTxnsData({this.to, this.amount, this.blockHeight, this.message});
  factory BlnTxnsData.fromJson(Map<String, dynamic> json){
    return new BlnTxnsData(
      to: json["to"] as String,
      amount: json["amount"] is int ?  (json["amount"] as int).toDouble() : json["amount"] is String ? double.parse(json["amount"]): json["amount"] as double,
      blockHeight: json["blockHeight"] as int,
      message: json["message"] is Map ? (json["message"] as Map)["message"] : json["message"],
    );
  }
  Map<String, dynamic> toJson() => {
    'amount': amount,
    'to': to,
    'message': message,
    'blockHeight': blockHeight
  };
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"to\":\"$to\"");
    sb.write(",\"amount\":\"$amount\"");
    sb.write(",\"blockHeight\":\"$blockHeight\"");
    sb.write(",\"message\":\"$message\"");
    sb.write('}');
    return sb.toString();
  }
}

// BlnLease
class BlnLease {
  String publicKey;
  int amount;
  int height;
  BlnLease({this.publicKey, this.amount, this.height});
  factory BlnLease.fromJson(Map<String, dynamic> json){
    return new BlnLease(
      amount: json["amount"] as int,
      height: json["height"] as int,
      publicKey: json["publicKey"] as String
    );
  }
  Map<String, dynamic> toJson() => {
    'publicKey': publicKey,
    'amount': amount,
    'height': height
  };
}

// BlnContact
class BlnContact {
  String name;
  String address;
  String remark;
  BlnContact({this.name, this.address, this.remark});
  factory BlnContact.fromJson(Map<String, dynamic> json){
    return new BlnContact(
      name: json["name"],
      address: json["address"],
      remark: json["remark"]
    );
  }
  Map<String, dynamic> toJson() => {
    'name': name,
    'address': address,
    'remark': remark
  };
  Map<String, dynamic> toMap() => {
    'name': name,
    'address': address,
    'remark': remark
  };
  factory BlnContact.fromMap(Map<String, dynamic> json){
    return new BlnContact(
      name: json["name"],
      address: json["address"],
      remark: json["remark"],
    );
  }
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write(",\"name\":\"$name\"");
    sb.write(",\"address\":\"$address\"");
    sb.write(",\"remark\":\"$remark\"");
    sb.write('}');
    return sb.toString();
  }
}

const Map<int, String> BlnTxData = {
  -1: "all",
  0: "transfer",
  1: "burn",
  2: "lease",
  3: "cancelLease",
  4: "bundle",
  5: "createHTLC",
  6: "unlockHTLC",
  7: "refund HTLC",
  8: "spend HTLC",
  9: "createMultisig",
  10: "spendMultisig",
  254: "posGenerated"
};

class BlnTxType {
  static const int all = -1;
  static const int transfer = 0;
  static const int burn = 1;
  static const int lease = 2;
  static const int cancelLease = 3;
  static const int bundle = 4;
  static const int createHTLC = 5;
  static const int unlockHTLC = 6;
  static const int refundHTLC = 7;
  static const int spendHTLC = 8;
  static const int createMultisig = 9;
  static const int spendMultisig = 10;
  static const int posGenerated = 254;
}

String getBlnAddress(){
  // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    String mnemonic = "method deer clutch swing gift animal body erode cage shrug attack divorce";
    // String randomMnemonic = bip39.generateMnemonic();
    
    String hash = Blake2bHash.hashUtf8String2HexString(mnemonic);

    Uint8List hashBytes = Utils.int8list2uint8list(Int8List.fromList(HEX.decode(hash)));
    Uint8List pk = Ed25519.publickey(blake2bHashFunc, hashBytes);
    Uint8List cvtBytes = convertBits(pk, 8, 5, true);
    Bech32Codec bech32codec = Bech32Codec();
    return bech32codec.encode(Bech32("blacknet", cvtBytes));
}

Uint8List blake2bHashFunc(Uint8List m) {
  Uint8List bytes = Uint8List(64);
  var b = Blake2b(512);
  b.update(m, 0, m.length);
  b.digest(bytes, 0);
  return bytes;
}

Uint8List convertBits(Uint8List data, int from, int to, bool pad) {
  Int32 acc = Int32.parseInt("0");
  Int32 bits = Int32.parseInt("0");
  Int32 maxv = (Int32.ONE << to) - 1;
  List<int> ret = [];
  
  for (int i = 0; i < data.length; i++) {
    int b = data[i] & 0xFF;
    if (b < 0) {
      return null;
    } else if (b >> from > 0) {
      return null;
    }

    acc = acc << from | b;
    bits += from;
    while (bits >= to) {
      bits -= to;
      Int32 tmp = acc >> bits.toInt();
      tmp = tmp & maxv;
      ret.add(tmp.toInt());
    }
  }

  if (pad && bits > 0) {
    Int32 tmp = acc << to - bits.toInt() & maxv;
    ret.add(tmp.toInt());
  } else if(bits > from || (acc << to - bits.toInt() & maxv != 0)) {
    return null;
  }
  return Uint8List.fromList(ret);
}