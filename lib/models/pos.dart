
// PosListItem
class PosListItem {
  String account;
  int allShares;
  double amount;
  int blockHeight;
  String fee;
  bool isPayout;
  String poolOwner;
  double reward;
  int shares;
  PosListItem({this.account, this.allShares, this.amount, this.blockHeight, this.fee, this.isPayout, this.poolOwner, this.reward, this.shares});
  factory PosListItem.fromJson(Map<String, dynamic> json){
    return new PosListItem(
      account: json["account"],
      fee: json["fee"],
      allShares: json["allShares"],
      amount: json["amount"].toDouble(),
      blockHeight: json["blockHeight"],
      isPayout: json["isPayout"],
      poolOwner: json["poolOwner"],
      reward: json["reward"],
      shares: json["shares"]
    );
  }
  Map<String, dynamic> toJson() => {
    'account': account,
    'fee': fee,
    'allShares': allShares,
    'amount': amount,
    'blockHeight': blockHeight,
    'isPayout': isPayout,
    'poolOwner': poolOwner,
    'reward': reward,
    'shares': shares
  };
  Map<String, dynamic> toMap() => {
    'account': account,
    'fee': fee,
    'allShares': allShares,
    'amount': amount,
    'blockHeight': blockHeight,
    'isPayout': isPayout,
    'poolOwner': poolOwner,
    'reward': reward,
    'shares': shares
  };
  factory PosListItem.fromMap(Map<String, dynamic> json){
    return new PosListItem(
      account: json["account"],
      fee: json["fee"],
      allShares: json["allShares"],
      amount: json["amount"],
      blockHeight: json["blockHeight"],
      isPayout: json["isPayout"],
      poolOwner: json["poolOwner"],
      reward: json["reward"],
      shares: json["shares"]
    );
  }
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"account\":\"$account\"");
    sb.write(",\"fee\":\"$fee\"");
    sb.write(",\"allShares\":\"$allShares\"");
    sb.write(",\"amount\":\"$amount\"");
    sb.write(",\"blockHeight\":\"$blockHeight\"");
    sb.write(",\"isPayout\":\"$isPayout\"");
    sb.write(",\"poolOwner\":\"$poolOwner\"");
    sb.write(",\"reward\":\"$reward\"");
    sb.write(",\"shares\":\"$shares\"");
    sb.write('}');
    return sb.toString();
  }
}

// PoolHashrate
class PoolHashrate {
  int hashrate;
  int number;
  int blockHeight;
  String blockHash;
  String time;
  String poolOwner;
  PoolHashrate({this.hashrate, this.number, this.blockHeight, this.blockHash, this.time, this.poolOwner});
  factory PoolHashrate.fromJson(Map<String, dynamic> json){
    return new PoolHashrate(
      hashrate: (json["hashrate"]).toInt(),
      number: json["number"],
      blockHeight: json["blockHeight"],
      blockHash: json["blockHash"],
      time: json["time"],
      poolOwner: json["poolOwner"]
    );
  }
  Map<String, dynamic> toJson() => {
    'hashrate': hashrate,
    'number': number,
    'blockHeight': blockHeight,
    'blockHash': blockHash,
    'time': time,
    'poolOwner': poolOwner
  };
  Map<String, dynamic> toMap() => {
    'hashrate': hashrate,
    'number': number,
    'blockHeight': blockHeight,
    'blockHash': blockHash,
    'time': time,
    'poolOwner': poolOwner
  };
  factory PoolHashrate.fromMap(Map<String, dynamic> json){
    return new PoolHashrate(
      hashrate: json["hashrate"],
      number: json["number"],
      blockHeight: json["blockHeight"],
      blockHash: json["blockHash"],
      time: json["time"],
      poolOwner: json["poolOwner"]
    );
  }
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"hashrate\":\"$hashrate\"");
    sb.write(",\"number\":\"$number\"");
    sb.write(",\"blockHeight\":\"$blockHeight\"");
    sb.write(",\"blockHash\":\"$blockHash\"");
    sb.write(",\"time\":\"$time\"");
    sb.write(",\"poolOwner\":\"$poolOwner\"");
    sb.write('}');
    return sb.toString();
  }
}


// PosWorkerHashrate
class PosWorkerHashrate {
  int hashrate = 0; // 账户权重
  int networkHashrate = 0; //全部权重
  int poolHashrate = 0; //权益池权重
  int stakingBalance = 0; //pos数量
  int balance = 0; //余额
  int profitEst = 0; //收益
  PosWorkerHashrate({this.hashrate, this.networkHashrate, this.poolHashrate, this.stakingBalance, this.balance, this.profitEst});
  factory PosWorkerHashrate.fromJson(Map<String, dynamic> json){
    return new PosWorkerHashrate(
      hashrate: json["hashrate"],
      networkHashrate: json["networkHashrate"],
      poolHashrate: json["poolHashrate"],
      stakingBalance: json["stakingBalance"],
      balance: json["balance"],
      profitEst: json["profitEst"]
    );
  }
  Map<String, dynamic> toJson() => {
    'hashrate': hashrate,
    'networkHashrate': networkHashrate,
    'poolHashrate': poolHashrate,
    'stakingBalance': stakingBalance,
    'balance': balance,
    'profitEst': profitEst
  };
  Map<String, dynamic> toMap() => {
    'hashrate': hashrate,
    'networkHashrate': networkHashrate,
    'poolHashrate': poolHashrate,
    'stakingBalance': stakingBalance,
    'balance': balance,
    'profitEst': profitEst
  };
  factory PosWorkerHashrate.fromMap(Map<String, dynamic> json){
    return new PosWorkerHashrate(
      hashrate: json["hashrate"],
      networkHashrate: json["networkHashrate"],
      poolHashrate: json["poolHashrate"],
      stakingBalance: json["stakingBalance"],
      balance: json["balance"],
      profitEst: json["profitEst"]
    );
  }
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"hashrate\":$hashrate");
    sb.write(",\"networkHashrate\":$networkHashrate");
    sb.write(",\"poolHashrate\":$poolHashrate");
    sb.write(",\"stakingBalance\":$stakingBalance");
    sb.write(",\"balance\":$balance");
    sb.write(",\"profitEst\":$profitEst");
    sb.write('}');
    return sb.toString();
  }
}

// Hashrate
class Hashrate {
  int networkhashrate;
  int workers;
  Hashrate({this.networkhashrate, this.workers});
  factory Hashrate.fromJson(Map<String, dynamic> json){
    return new Hashrate(
      networkhashrate: json["networkhashrate"],
      workers: json["workers"]
    );
  }
  Map<String, dynamic> toJson() => {
    'networkhashrate': networkhashrate,
    'workers': workers
  };
  Map<String, dynamic> toMap() => {
    'networkhashrate': networkhashrate,
    'workers': workers
  };
  factory Hashrate.fromMap(Map<String, dynamic> json){
    return new Hashrate(
      networkhashrate: json["networkhashrate"],
      workers: json["workers"]
    );
  }
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"networkhashrate\":\"$networkhashrate\"");
    sb.write(",\"workers\":\"$workers\"");
    sb.write('}');
    return sb.toString();
  }
}


// PosWorker
class PosWorker {
  int hashrate;
  int payouts;
  double paid;
  double totalReward;
  String address;
  String poolOwner;
  PosWorker({this.hashrate, this.payouts, this.paid, this.totalReward, this.address, this.poolOwner});
  factory PosWorker.fromJson(Map<String, dynamic> json){
    return new PosWorker(
      hashrate: json["hashrate"],
      payouts: json["payouts"],
      paid: json["paid"].toDouble(),
      totalReward: json["total_reward"].toDouble(),
      address: json["address"],
      poolOwner: json["poolOwner"]
    );
  }
  Map<String, dynamic> toJson() => {
    'hashrate': hashrate,
    'payouts': payouts,
    'paid': paid,
    'total_reward': totalReward,
    'address': address,
    'poolOwner': poolOwner
  };
  Map<String, dynamic> toMap() => {
    'hashrate': hashrate,
    'payouts': payouts,
    'paid': paid,
    'total_reward': totalReward,
    'address': address,
    'poolOwner': poolOwner
  };
  factory PosWorker.fromMap(Map<String, dynamic> json){
    return new PosWorker(
      hashrate: json["hashrate"],
      payouts: json["payouts"],
      paid: json["paid"],
      totalReward: json["total_reward"],
      address: json["address"],
      poolOwner: json["poolOwner"]
    );
  }
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"hashrate\":\"$hashrate\"");
    sb.write(",\"payouts\":\"$payouts\"");
    sb.write(",\"paid\":\"$paid\"");
    sb.write(",\"total_reward\":\"$totalReward\"");
    sb.write(",\"address\":\"$address\"");
    sb.write(",\"poolOwner\":\"$poolOwner\"");
    sb.write('}');
    return sb.toString();
  }
}
