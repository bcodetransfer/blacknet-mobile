import 'package:fixnum/fixnum.dart';

class Response {
  int code;
  var data;
  String message;
  Response(this.code, this.data, this.message);
}

class GeneralResponse {
  int code;
  String body;
  GeneralResponse(this.code, this.body);
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"code\":\"$code\"");
    sb.write(",\"body\":\"$body\"");
    sb.write('}');
    return sb.toString();
  }
}

class BlnResponse {
  int code;
  String body;
  BlnResponse(this.code, this.body);
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"code\":\"$code\"");
    sb.write(",\"body\":\"$body\"");
    sb.write('}');
    return sb.toString();
  }
}