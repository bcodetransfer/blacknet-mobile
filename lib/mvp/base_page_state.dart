
import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';
import 'package:blacknet/widgets/progress_dialog.dart';

import 'base_page_presenter.dart';
import 'IView.dart';

import 'package:local_auth/local_auth.dart';
import 'package:local_auth/auth_strings.dart';

abstract class BasePageState<T extends StatefulWidget, V extends BasePagePresenter> extends State<T> implements IView {

  V presenter;
  
  BasePageState() {
    presenter = createPresenter();
    presenter.view = this;
  }
  
  V createPresenter();

  @override
  BuildContext getContext() {
    return context;
  }
  
  @override
  void closeProgress() {
    if (mounted && _isShowDialog) {
      _isShowDialog = false;
      OkNavigator.goBack(context);
    }
  }

  bool _isShowDialog = false;

  final _localAuth = LocalAuthentication();

  @override
  void showProgress([bool transparency, String text]) {
    /// 避免重复弹出
    if (mounted && !_isShowDialog) {
      _isShowDialog = true;
      try{
        showTransparentDialog(
            context: context,
            barrierDismissible: false,
            builder:(_) {
              return WillPopScope(
                onWillPop: () async {
                  // 拦截到返回键，证明dialog被手动关闭
                  _isShowDialog = false;
                  return Future.value(true);
                },
                child: ProgressDialog(
                  transparency: transparency,
                  text: text
                )
              );
            }
        );
      } catch(e) {
        /// 异常原因主要是页面没有build完成就调用Progress。
        print(e);
      }
    }
  }

  @override
  void showToast(String string) {
    Toast.show(string);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    presenter?.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
    presenter?.dispose();
  }

  @override
  void deactivate() {
    super.deactivate();
    presenter?.deactivate();
  }

  @override
  void didUpdateWidget(T oldWidget) {
    super.didUpdateWidget(oldWidget);
    didUpdateWidgets<T>(oldWidget);
  }

  @override
  void initState() {
    super.initState();
    presenter?.initState();
  }

  void didUpdateWidgets<W>(W oldWidget) {
    presenter?.didUpdateWidgets<W>(oldWidget);
  }

  showImportButtonMenu() async{
    switch (await showModalBottomSheet<String>(
      context: context,
      builder: (BuildContext context) {
          return SafeArea(child: 
            Container(
              height: ThemeLayout.height(100),
              padding: ThemeLayout.top(10),
              child:
                ListView(
                  children: [
                    ListTile(
                      title: Text(S.of(context).importInputMnemonic,
                        style: TextStyle(
                          color: Theme.of(context).accentColor
                        ),
                      ),
                      leading: new CircleAvatar(
                          child: new Icon(Icons.list),
                          foregroundColor: Theme.of(context).accentColor,
                          backgroundColor: Theme.of(context).backgroundColor),
                      onTap: () {
                        OkNavigator.goBackWithParams(context, "mnemonic");
                      },
                    )
                  ]
                )
            )
          );
      },
    )) {
      case "mnemonic":
        OkNavigator.push(context, AccountRouter.accountImportPage1);
        break;
      default:
    }
  }

  Future showConfirmDialog(Widget title, Widget content) async {
    return await showDialog<bool>(
      context: context,
      barrierDismissible: false,//// user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: title,
          content: content,
          actions: <Widget>[
            FlatButton(
              child: Text(S.of(context).cancel),
              onPressed: () {
                OkNavigator.goBackWithParams(context, false);
              },
            ),
            FlatButton(
              child: Text(S.of(context).confirm),
              onPressed: () {
                OkNavigator.goBackWithParams(context, true);
              },
            ),
          ],
        );
      },
    );
  }

  Widget errorText([String text]){
    if (text != null) {
      return Text(text, 
        textAlign: TextAlign.center,
        style: TextStyles.textError14
      );
    }
    return Text("", 
          textAlign: TextAlign.center,
          style: TextStyles.textError14
        );
  }

  // showWarning show
  showWarning([Widget title, Widget content]){
    title = title != null ? title : Text(S.of(context).invalidMnemonic, 
      textAlign: TextAlign.center,
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 20
      ),
    );
    content = content != null ? content : Text(S.of(context).invalidMnemonicDesc,
      textAlign: TextAlign.center
    );

    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
        title: ListTile(
          title: Icon(Icons.warning,
            color: Theme.of(context).accentColor,
          ),
          subtitle: Padding(padding: ThemeLayout.paddingTop(10),
            child: title
          )
        ),
        children: <Widget>[
          ListTile(
            title: content,
          ),
          SimpleDialogOption(
            onPressed: () { Navigator.pop(context, false); },
            child: Text(S.of(context).cancel,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).accentColor,
              ),
            ),
          ),
        ],
      );
      }
    );
  }

  showLocalAuthToggle([bool toggle, Widget title, Widget content]) async{
    String type = await getBiometricType();
    if (type == null) {
      // type = "Face ID";
      // 直接保存密码
      return false;
    }
    title = title != null ? title : Text(toggle ? S.of(context).disableSavePassword: S.of(context).useTypeSecurity.replaceAll("{type}", type), 
      textAlign: TextAlign.center,
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 20
      ),
    );
    content = content != null ? content : Text(toggle ? S.of(context).ifDisableTypeSecurity.replaceAll("{type}", type): S.of(context).ifEnableTypeSecurity.replaceAll("{type}", type),
      textAlign: TextAlign.center
    );
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
        title: ListTile(
          title: Icon(Icons.face,
            color: Theme.of(context).accentColor,
          ),
          subtitle: Padding(padding: ThemeLayout.paddingTop(10),
            child: title
          )
        ),
        children: <Widget>[
          ListTile(
            title: content,
          ),
          ListTile(
            title: RaisedButton(
              child: Text(toggle ? S.of(context).continues : S.of(context).enable+"${type}"),
              onPressed: (){
                Navigator.pop(context, true);
              }
            ),
          ),
          SimpleDialogOption(
            onPressed: () { Navigator.pop(context, false); },
            child: Text(S.of(context).cancel,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).accentColor,
              ),
            ),
          ),
        ],
      );
      }
    );
  }

  Future<String> getBiometricType() async{
    List<BiometricType> availableBiometrics =
        await _localAuth.getAvailableBiometrics();
    if (availableBiometrics.contains(BiometricType.face)) {
        return "Face ID";
    } else if (availableBiometrics.contains(BiometricType.fingerprint)) {
        return "Touch ID";
    }
    return null;
  }
  Future<bool> canCheckBiometrics() async {
    return await _localAuth.canCheckBiometrics;
  }
  // local auth
  Future<bool> showLocalAuth() async {
    
    bool canCheckBiometrics = await _localAuth.canCheckBiometrics;

    Log.e('canCheckBiometrics: $canCheckBiometrics');
    List<BiometricType> availableBiometrics =
        await _localAuth.getAvailableBiometrics();

    Log.e('availableBiometrics: $availableBiometrics');
    
    if (availableBiometrics.contains(BiometricType.face)) {
        // Face ID.
        Log.e("face id");
    } else if (availableBiometrics.contains(BiometricType.fingerprint)) {
        // Touch ID.
        Log.e("touch id");
    } else {
        Log.e("password without face/touch id");
        return false;
    }

    bool authResult = false;

    try {
      const iosStrings = const IOSAuthMessages(
        cancelButton: 'cancel',
        goToSettingsButton: 'settings',
        goToSettingsDescription: 'Please set up your Touch ID.',
        lockOut: 'Please reenable your Touch ID');
      authResult = await _localAuth.authenticateWithBiometrics(
          localizedReason: 'Please authenticate to show account balance',
          useErrorDialogs: false,
          iOSAuthStrings: iosStrings);
    } catch (e) {
      Log.e("_localAuth.authenticateWithBiometrics: $e");
    }
    return authResult;
  } 
}