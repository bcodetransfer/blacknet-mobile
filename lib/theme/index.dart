export 'package:blacknet/theme/default.dart';

export 'package:blacknet/theme/layout.dart';

export 'package:blacknet/theme/themes.dart';

import 'package:blacknet/theme/themes.dart';
import 'package:blacknet/theme/default.dart';
import 'package:flutter/material.dart';

class ThemeDatas {

  static Themes currentTheme = Themes.darker;

  static Themes defaultTheme = Themes.darker;

  // themedatas
  static List<Themes> supportThemes = [
    Themes.darker,
    Themes.blue,
    Themes.deepPurple,
    Themes.red,
    Themes.teal,
    Themes.green,
    Themes.indigo,
    Themes.purple,
    Themes.deepPurple
  ];

  static Map<Themes, String> supportThemeMaps = {
    Themes.darker: "darker", 
    Themes.red   :  "red", 
    Themes.teal  : "teal", 
    Themes.blue : "blue",
    Themes.green : "green",
    Themes.indigo : "indigo",
    Themes.purple : "purple",
    Themes.deepPurple : "deepPurple"
  };

  static Map<String, ThemeData> supportThemeDatas = {
    "darker": ThemeData(
      primarySwatch: ThemeColor.darker,
      platform: TargetPlatform.android
    ), 
    "red": ThemeData(
      primarySwatch: Colors.red,
      platform: TargetPlatform.android
    ), 
    "teal": ThemeData(
      primarySwatch: Colors.teal,
      platform: TargetPlatform.android
    ), 
    "blue": ThemeData(
      primarySwatch: Colors.blue,
      platform: TargetPlatform.android
    ), 
    "green": ThemeData(
      primarySwatch: Colors.green,
      platform: TargetPlatform.android
    ), 
    "indigo": ThemeData(
      primarySwatch: Colors.indigo,
      platform: TargetPlatform.android
    ),
    "purple": ThemeData(
      primarySwatch: Colors.purple,
      platform: TargetPlatform.android
    ), 
    "deepPurple": ThemeData(
      primarySwatch: Colors.deepPurple,
      platform: TargetPlatform.android
    )
  };

  static ThemeData defaultThemeData = ThemeData(
    brightness: Brightness.dark,
    primarySwatch: ThemeColor.darker,
    cursorColor: ThemeColor.gold,
    accentColor: ThemeColor.gold, //突出颜色
    primaryColor: ThemeColor.darker, //主色
    bottomAppBarColor: ThemeColor.darker,
    backgroundColor: ThemeColor.lighter,
    cardColor: ThemeColor.lighter,

    // highlightColor: ThemeColor.gold, //被选中高粱颜色
    // splashColor: ThemeColor.gold, // 点击效果颜色
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,

    hintColor: ThemeColor.darker,
    errorColor: Colors.redAccent, //错误颜色
    // appbar
    appBarTheme: AppBarTheme(
      actionsIconTheme: IconThemeData(
        color: ThemeColor.grey,
        size: 18
      ),
      iconTheme: IconThemeData(
        color: ThemeColor.grey,
        size: 18
      ),
      textTheme: TextTheme(
        title: TextStyle(color: ThemeColor.grey, fontSize: 18)
      )
    ),
    // 按钮主体
    buttonTheme: ButtonThemeData(
      buttonColor: ThemeColor.darker.shade600, //按钮颜色
      disabledColor: ThemeColor.lighter, //禁用颜色
      textTheme: ButtonTextTheme.accent
    ),
    // tabBarTheme
    tabBarTheme: TabBarTheme(
      labelColor: ThemeColor.grey,
      unselectedLabelColor: ThemeColor.grey
    ),
    // icon
    iconTheme: IconThemeData(
      color: ThemeColor.grey
    ),
    // slider
    sliderTheme: SliderThemeData(
      activeTrackColor: ThemeColor.gold,
      inactiveTrackColor: ThemeColor.gold,
      activeTickMarkColor: ThemeColor.gold,
      inactiveTickMarkColor: ThemeColor.gold,
      thumbColor: ThemeColor.gold, //按钮颜色
      // overlayColor: ThemeColor.gold.shade400 //散开颜色
    ),
    // 
    fontFamily: 'Menlo',
    // 文字主体
    textTheme: TextTheme(
      display1: TextStyle(color: ThemeColor.grey, fontSize: 16), // grey
      display2: TextStyle(color: ThemeColor.gold, fontSize: 16), // gold
      display3: TextStyle(color: ThemeColor.violet, fontSize: 16), // violet
      display4: TextStyle(color: ThemeColor.darker, fontSize: 16), // darker
      body1: TextStyle(color: ThemeColor.grey),
      body2: TextStyle(color: ThemeColor.lighter),
      overline: TextStyle(color: ThemeColor.grey),
      headline: TextStyle(color: ThemeColor.grey),
      subhead: TextStyle(color: ThemeColor.grey), //输入文字颜色等等
      subtitle: TextStyle(color: ThemeColor.grey),
      title: TextStyle(color: ThemeColor.grey),
      caption: TextStyle(color: ThemeColor.grey),
      button: TextStyle(color: ThemeColor.grey)
    ),
    // inputDecorationTheme
    inputDecorationTheme: InputDecorationTheme(
      
    ),
    platform: TargetPlatform.iOS,
    pageTransitionsTheme: PageTransitionsTheme(
      builders: {
        TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      }
    )
  );
}

// ThemeData(
    // primarySwatch: Colors.blue,
    // platform: TargetPlatform.android,
    // backgroundColor: ThemeColor.darker, //背景颜色
    // accentColor: ThemeColor.grey, // 突出颜色顶部的文本和图标的颜色 文字颜色
    // accentColor: ThemeColor.gold, // 突出颜色顶部的文本和图标的颜色 文字颜色
    // // accentTextTheme: TextTheme(button: TextStyle(color: ThemeColor.gold)),
    // brightness: Brightness.dark,
    // primaryColor: ThemeColor.darker, // App主要部分的背景色（ToolBar,Tabbar等）。
    // // primaryColor: ThemeColor.darker,
    // // primarySwatch: ThemeColor.gold,
    // // splashFactory: const NoSplashFactory(),
    // // primaryColor: ThemeColor.gold,
    // // hintColor: ThemeColor.grey,
    // // primaryColorDark: ThemeColor.darker,
    // // primaryColorLight: ThemeColor.lighter,
    // buttonColor: ThemeColor.darker.shade400, // 按钮背景色
    // // buttonColor: ThemeColor.gold, // 按钮背景色
    // backgroundColor: ThemeColor.darker, //背景颜色
    // primaryColor: ThemeColor.gold,
  // );