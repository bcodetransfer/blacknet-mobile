
import 'package:flutter/material.dart';

import 'package:blacknet/res/index.dart';

class ThemeColor {
  // gold-text
  static MaterialColor gold = MaterialColor(hexToColor("#E8BA3F").value, {
    50: hexToColor("#FCF7E8"),
    100: hexToColor("#F8EAC5"),
    200: hexToColor("#F4DD9F"),
    300: hexToColor("#EFCF79"),
    400: hexToColor("#EBC45C"),
    500: hexToColor("#E8BA3F"),
    600: hexToColor("#E5B339"),
    700: hexToColor("#E2AB31"),
    800: hexToColor("#DEA329"),
    900: hexToColor("#D8941B")
  });
  // darker
  static MaterialColor darker = MaterialColor(0xFF2D2D2D, {
    50: hexToColor("#E6E6E6"),
    100: hexToColor("#C0C0C0"),
    200: hexToColor("#969696"),
    300: hexToColor("#6C6C6C"),
    400: hexToColor("#4D4D4D"),
    500: hexToColor("#2D2D2D"),
    600: hexToColor("#282828"),
    700: hexToColor("#222222"),
    800: hexToColor("#1C1C1C"),
    900: hexToColor("#111111"),
  });
  // lighter
  static MaterialColor lighter = MaterialColor(hexToColor("#3B3B3B").value, {
    50: hexToColor("#E7E7E7"),
    100: hexToColor("#C4C4C4"),
    200: hexToColor("#9D9D9D"),
    300: hexToColor("#767676"),
    400: hexToColor("#585858"),
    500: hexToColor("#3B3B3B"),
    600: hexToColor("#353535"),
    700: hexToColor("#2D2D2D"),
    800: hexToColor("#262626"),
    900: hexToColor("#191919")
  });
  // grey
  static MaterialColor grey = MaterialColor(hexToColor("#ADADAD").value, {
    50: hexToColor("#F5F5F5"),
    100: hexToColor("#E6E6E6"),
    200: hexToColor("#D6D6D6"),
    300: hexToColor("#C6C6C6"),
    400: hexToColor("#B9B9B9"),
    500: hexToColor("#ADADAD"),
    600: hexToColor("#A6A6A6"),
    700: hexToColor("#9C9C9C"),
    800: hexToColor("#939393"),
    900: hexToColor("#838383")
  });
  // violet
  static MaterialColor violet = MaterialColor(hexToColor("#3CA2A2").value, {
    50: hexToColor("#E8F4F4"),
    100: hexToColor("#C5E3E3"),
    200: hexToColor("#9ED1D1"),
    300: hexToColor("#77BEBE"),
    400: hexToColor("#59B0B0"),
    500: hexToColor("#3CA2A2"),
    600: hexToColor("#369A9A"),
    700: hexToColor("#2E9090"),
    800: hexToColor("#278686"),
    900: hexToColor("#1A7575")
  });

  // Default Color
  static Color defaultColor = darker;

  // Optional Color
  static List<Color> supportColors = [
    defaultColor,
    // gold,
    // Colors.amber,
    Colors.red,
    // Colors.cyan,
    Colors.teal,
    Colors.blue,
    // Colors.lightBlue,
    Colors.green,
    // Colors.lime,
    Colors.indigo,
    Colors.purple,
    Colors.deepPurple,
  ];
  
  // Current Theme Color
  static Color currentColorTheme = defaultColor;
}

// :root {
//     --darker-grey-bg: #2D2D2D;
//     --lighter-grey-bg: #3B3B3B;
//     --gold-text: #E8BA3F;
//     --grey-text: #ADADAD;
// }

// body {
//     background: #3B3B3B;
// }


class NoSplashFactory extends InteractiveInkFeatureFactory {
  const NoSplashFactory();

  @override
  InteractiveInkFeature create(
      {MaterialInkController controller,
      RenderBox referenceBox,
      Offset position,
      Color color,
      TextDirection textDirection,
      bool containedInkWell = false,
      rectCallback,
      BorderRadius borderRadius,
      ShapeBorder customBorder,
      double radius,
      onRemoved}) {
    return new NoSplash(
      controller: controller,
      referenceBox: referenceBox,
    );
  }
}

class NoSplash extends InteractiveInkFeature {
  NoSplash({
    @required MaterialInkController controller,
    @required RenderBox referenceBox,
  })  : assert(controller != null),
        assert(referenceBox != null),
        super(
          controller: controller,
          referenceBox: referenceBox,
        );

  @override
  void paintFeature(Canvas canvas, Matrix4 transform) {}
}