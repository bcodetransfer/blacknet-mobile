import 'package:blacknet/components/progress_dialog/progress_dialog.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth/auth_strings.dart';

class LocalAuthenticationService {
  final _auth = LocalAuthentication();
  bool isAuthenticated = false;

  Future<bool> auth([String msg]) async {
    

    if (msg != null) {
      showProgressDialog(loadingText: msg);
    }
    bool authResult = false;

    try {
      const iosStrings = const IOSAuthMessages(
        cancelButton: 'cancel',
        goToSettingsButton: 'settings',
        goToSettingsDescription: 'Please set up your Touch ID.',
        lockOut: 'Please reenable your Touch ID');
      authResult = await _auth.authenticateWithBiometrics(
          localizedReason: 'Please authenticate to show account balance',
          useErrorDialogs: false,
          iOSAuthStrings: iosStrings);
    } catch (e) {
      
      return true;
    }

    return authResult;
  }
}
