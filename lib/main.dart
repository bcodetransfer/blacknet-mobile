import 'package:fluro/fluro.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:blacknet/components/index.dart';
// localizations
import 'package:flutter_localizations/flutter_localizations.dart'; 
import 'package:oktoast/oktoast.dart';
import 'package:provider/provider.dart';
//privider
import 'package:blacknet/provider/datacenter.dart';

import 'package:blacknet/provider/global.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/tx.dart';
// utils
import 'package:blacknet/utils/sp.dart';
// router
import 'package:blacknet/routers/routers.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';
// main page
import 'package:blacknet/pages/index.dart';

import 'package:blacknet/services/service_locator.dart';

void main(){
  ///
  /// 强制竖屏
  /// 
    WidgetsFlutterBinding.ensureInitialized();
  /// 
  /// 
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
  .then((_) {

    setupLocator();
    
    runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: DataCenterProvider()),
          ChangeNotifierProvider.value(value: GlobalProvider()),
          ChangeNotifierProvider.value(value: AccountProvider()),
          ChangeNotifierProvider.value(value: TxProvider()),
        ],
        child: BlacknetApp()
      )
    );
  });
}

class BlacknetApp extends StatefulWidget {
  @override
  _BlacknetApp createState() => _BlacknetApp();

}

class _BlacknetApp extends State<BlacknetApp>  {

  @override
  void initState() {
    super.initState();
    _initAsync();
  }

  void _initAsync() async {
    if (!mounted) return;
    await SpHelper.getInstance();
    // 同步初始化
    Provider.of<GlobalProvider>(context).syncTheme();
    Provider.of<AccountProvider>(context).syncCurrentBln();
  }
  
  @override
  Widget build(BuildContext context) {
    // Route
    final router = Router();
    Routes.generate(router);
    // theme
    ThemeData themeData = Provider.of<GlobalProvider>(context).getTheme();

    return OKToast(
      dismissOtherOnShow: false,
      child: Consumer<GlobalProvider>(
        builder: (_, global, __) {
          return ProgressDialog(
            dismissible: false,
            opacity: 0,
            orientation: ProgressOrientation.vertical,
            child: MaterialApp(
              title: 'Blacknet',
              // 国际化
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate
              ],
              localeResolutionCallback: (Locale locale, Iterable<Locale> supportedLocales) {
                if(!supportedLocales.contains(locale)){
                  return S.delegate.formatLocale(locale);
                }
                return locale;
              },
              supportedLocales: S.delegate.supportedLocales,
              locale: global.getLanguage(),
              theme: global.getTheme(),
              debugShowCheckedModeBanner: false,
              home: IndexPage(),
              onGenerateRoute: Routes.router.generator,
            )
          );
        },
      ),
      backgroundColor: themeData.backgroundColor,
      textStyle: TextStyle(color: themeData.accentColor),
      textPadding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
      radius: 5.0,
      position: ToastPosition.top
    );
  }
}
