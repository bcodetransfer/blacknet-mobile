import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:blacknet/res/index.dart';

/// 加载中的弹框
class ProgressDialog extends Dialog{

  const ProgressDialog({
    Key key,
    bool transparency,
    String text
  }): transparency = transparency ?? true,
      text = text ?? null,
      super(key: key);

  final bool transparency;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Material(
      type: transparency ? MaterialType.transparency : MaterialType.canvas,
      child: Center(
        child: Container(
          height: 100.0,
          width: 100.0,
          decoration: ShapeDecoration(
              color: Colours.darker,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))
              )
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Theme(
                data: ThemeData(
                  cupertinoOverrideTheme: CupertinoThemeData(
                    brightness: Brightness.dark // 局部指定夜间模式，加载圈颜色会设置为白色
                  )
                ),
                // child: const CupertinoActivityIndicator(radius: 14.0),
                child: CircularProgressIndicator(strokeWidth: 3.0, valueColor: AlwaysStoppedAnimation<Color>(Colours.gold)),
              ),
              text != null ? Column(children: [Gaps.vGap8, Text(text)]) : Gaps.empty
            ],
          ),
        ),
      ),
    );
  }
}

// class ProgressDialog extends StatelessWidget {
//   //子布局
//   final Widget child;

//   //加载中是否显示
//   final bool loading;

//   //进度提醒内容
//   final String text;

//   //加载中动画
//   final Widget progress;

//   //背景透明度
//   final double alpha;

//   //字体颜色
//   final Color textColor;

//   //字体背景颜色
//   final Color textBackgroundColor;

//   ProgressDialog(
//       {Key key,
//       @required this.loading,
//       this.text,
//       this.progress,
//       this.alpha = 0.2,
//       this.textColor = Colors.white,
//       this.child,
//       this.textBackgroundColor,
//       })
//       : assert(loading != null),
//         super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     List<Widget> widgetList = [];
//     if (child != null) {
//       widgetList.add(child);
//     }
//     Widget progressChild = progress != null ? progress : CircularProgressIndicator(backgroundColor: Theme.of(context).backgroundColor);
//     if (loading) {
//       Widget layoutProgress;
//       if (text == null) {
//         layoutProgress = Center(
//           child: progressChild
//         );
//       } else {
//         layoutProgress = Center(
//           child: Container(
//             padding: const EdgeInsets.all(20.0),
//             decoration: BoxDecoration(
//                 color: this.textBackgroundColor != null ? this.textBackgroundColor : Theme.of(context).accentColor,
//                 borderRadius: BorderRadius.circular(4.0)),
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               crossAxisAlignment: CrossAxisAlignment.center,
//               mainAxisSize: MainAxisSize.min,
//               children: <Widget>[
//                 progressChild,
//                 Container(
//                   padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0),
//                   child: Text(
//                     text,
//                     style: TextStyle(color: textColor, fontSize: 16.0),
//                   ),
//                 )
//               ],
//             ),
//           ),
//         );
//       }
//       widgetList.add(Opacity(
//         opacity: alpha,
//         child: new ModalBarrier(dismissible:false,color: Theme.of(context).accentColor),
//       ));
//       widgetList.add(layoutProgress);
//     }
//     return Stack(
//       children: widgetList,
//     );
//   }
// }