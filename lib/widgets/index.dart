export 'package:blacknet/widgets/button.dart';
export 'package:blacknet/widgets/progress_dialog.dart';
export 'package:blacknet/widgets/my_flexiblespacebar.dart';
export 'package:blacknet/widgets/more.dart';
export 'package:blacknet/widgets/state_type.dart';
export 'package:blacknet/widgets/password.dart';
export 'package:blacknet/widgets/qrcode_view.dart';
export 'package:blacknet/widgets/page.dart';
export 'package:blacknet/widgets/browser.dart';
export 'package:blacknet/widgets/MeSliverAppBar.dart';





import 'package:flutter/material.dart';

Widget my_appBar(Widget title, Color shadow, Color backgroudColor, [List<Widget> actions]){
  return PreferredSize(
    child: Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: shadow,
          offset: Offset(0, 1.0),
          blurRadius: 0.0,
        )
      ]),
      child: AppBar(
        elevation: 0.0,
        title: title,
        backgroundColor: backgroudColor,
        actions: actions.length > 0 ? actions : [],
      ),
    ),
    preferredSize: Size.fromHeight(kToolbarHeight),
  );
}