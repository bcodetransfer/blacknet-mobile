
import 'package:flutter/material.dart';

class MyPage extends StatelessWidget {

  const MyPage({
    Key key,
    this.child
  }): super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
            // 触摸收起键盘
            FocusScope.of(context).requestFocus(FocusNode());
        },
        child: child
    );
  }
}
