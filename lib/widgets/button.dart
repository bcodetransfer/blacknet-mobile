
import 'package:flutter/material.dart';
import 'package:blacknet/res/index.dart';
// import 'package:flutter_deer/util/theme_utils.dart';

class MyButton extends StatelessWidget {

  const MyButton({
    Key key,
    this.text: "",
    @required this.onPressed,
  }): super(key: key);

  final String text;
  final VoidCallback onPressed;

  void unfocusText(context){
    FocusScope.of(context).requestFocus(FocusNode());
  }
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: (){
        unfocusText(context);
        onPressed();
      },
      // child: Text(text, style: TextStyle(fontSize: Dimens.font_sp18))
      // textColor: Theme.of(context).buttonColor,
      // color: Theme.of(context).accentColor,
      // disabledTextColor: Theme.of(context).accentColor,
      // disabledColor: Theme.of(context).buttonColor,
      // shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: <Widget>[
          Container(
            height: 40,
            width: double.infinity,
            alignment: Alignment.center,
            child: Text(text, style: TextStyle(fontSize: Dimens.font_sp18)),
          ),
        ],
      ),
    );
  }
}
