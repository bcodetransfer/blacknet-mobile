import 'package:intl/intl.dart';
import 'dart:math';
// import 'package:fixnum/fixnum.dart';

String fomartBalance(double balance, [int len]){
  if(len==null){
    len = 8;
  }
  // return double.parse((balance / pow(10, 8)).toDouble().toStringAsFixed(len).toString()).toString();
  return (balance / pow(10, 8)).toDouble().toStringAsFixed(len).toString();
}

double realBalance(double balance){
  return (balance / pow(10, 8)).toDouble();
}

String formatDate(DateTime now){
  return new DateFormat('yyyy-MM-dd hh:mm:ss').format(now);
}

DateTime parseTime(String time){
  if(time.length == 10){
    return DateTime.fromMicrosecondsSinceEpoch(int.parse(time)*1e6.toInt());
  }
  if(time.length == 13){
    return DateTime.fromMicrosecondsSinceEpoch(int.parse(time)*1e3.toInt());
  }
  return DateTime.fromMicrosecondsSinceEpoch(int.parse(time));
}

String shortAddress(String address, [int len]){
  // print(address.substring(address.length - 16, address.length));
  if(len==null){
    len = 16;
  }
  return address.substring(0,len) +"..."+address.substring(address.length - len, address.length);
}