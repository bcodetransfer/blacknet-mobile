import 'dart:async';
import 'package:flutter/services.dart';
import 'dart:ui' as ui;

Future<ui.Image> loadOverlayLogoImage() async {
  final completer = Completer<ui.Image>();
  final byteData = await rootBundle.load('assets/images/logo.png');
  ui.decodeImageFromList(byteData.buffer.asUint8List(), completer.complete);
  return completer.future;
}