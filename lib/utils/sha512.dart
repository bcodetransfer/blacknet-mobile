import 'dart:convert';

import 'package:crypto/crypto.dart';

class Sha512Util {
  static String hash(String plainText) {
    var data = utf8.encode(plainText);
    var digest = sha512.convert(data);
    return digest.toString();
  }
}
