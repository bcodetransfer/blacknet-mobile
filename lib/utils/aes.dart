import 'package:encrypt/encrypt.dart';

class AESUtil {
  static String encryptToBase64(String plainText, String keyText) {
    final key = Key.fromUtf8(keyText);
    final iv = IV.fromLength(16);

    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));

    return encrypter.encrypt(plainText, iv: iv).base64;
  }

  static String decryptFromBase64(String encrypted, String keyText) {
    final key = Key.fromUtf8(keyText);
    final iv = IV.fromLength(16);

    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));

    return encrypter.decrypt64(encrypted, iv: iv);
  }
}