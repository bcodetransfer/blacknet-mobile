import 'package:flustars/flustars.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:blacknet/models/index.dart';
class SpHelper {
  /// T 用于区分存储类型
  /// Example.
  /// SpHelper.putObject<int>(key, value);
  /// SpHelper.putObject<double>(key, value);
  /// SpHelper.putObject<bool>(key, value);
  /// SpHelper.putObject<String>(key, value);
  /// SpHelper.putObject<List>(key, value);
  ///
  /// SpHelper.putObject(key, UserModel);
  ///
  static Future<SpUtil> getInstance() async{
    return await SpUtil.getInstance();
  }
  static Future<bool> clear() async{
    final pref = await SharedPreferences.getInstance();
    return await pref.clear();
  }
  static void putObject<T>(String key, Object value) {
    switch (T) {
      case int:
        SpUtil.putInt(key, value);
        break;
      case double:
        SpUtil.putDouble(key, value);
        break;
      case bool:
        SpUtil.putBool(key, value);
        break;
      case String:
        SpUtil.putString(key, value);
        break;
      case List:
        SpUtil.putStringList(key, value);
        break;
      default:
        SpUtil.putObject(key, value);
        break;
    }
  }

  static Object getObject<T>(String key) {
    Map map = SpUtil.getObject(key);
    if (map == null) return null;
    Object obj;
    switch (T) {
      case LanguageModel:
        obj = LanguageModel.fromJson(map);
        break;
      case Bln:
        obj = Bln.fromJson(map);
        break;
      case BlnBalance:
        obj = BlnBalance.fromJson(map);
        break;
      default:
        break;
    }
    return obj;
  }

  static List<Bln> getBlnList<T>(String key) {
    List<Map> list = SpUtil.getObjectList(key);
    if (list == null) {
      return new List();
    }
    return list.map((i)=>Bln.fromJson(i)).toList();
  }

  static Future<bool> putBlnList<T>(String key, List<Bln> list) {
    return SpUtil.putObjectList(key, list);
  }

}