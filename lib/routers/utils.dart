import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluro/fluro.dart';

import 'package:blacknet/widgets/index.dart';
import 'package:blacknet/routers/routers.dart';

class OkNavigator {
  static webview(BuildContext context, String title, String url){
    FocusScope.of(context).unfocus();
    Navigator.of(context)
      .push(new FadeAnimationMaterialPageRoute(builder: (_) {
      return new Browser(
        url: url,
        title: title,
      );
    }));
  }

    /// 返回
  static void goBack(BuildContext context) {
    FocusScope.of(context).unfocus();
    Navigator.pop(context);
  }

  /// 带参数返回
  static void goBackWithParams(BuildContext context, result) {
    FocusScope.of(context).unfocus();
    Navigator.pop(context, result);
  }

  static push(BuildContext context, String path,
      {bool replace = false, bool clearStack = false}) {
    FocusScope.of(context).unfocus();
    Routes.router.navigateTo(context, path, replace: replace, clearStack: clearStack, transition: TransitionType.native);
  }

  static pushReplacementNamed(BuildContext context, String path) {
      // Navigator.pushReplacementNamed(context, name);
      FocusScope.of(context).unfocus();
    Routes.router.navigateTo(context, path, replace: true, clearStack: true, transition: TransitionType.native);
  }

  static pushResult(BuildContext context, String path, Function(Object) function,
      {bool replace = false, bool clearStack = false}) {
    FocusScope.of(context).unfocus();
    Routes.router.navigateTo(context, path, replace: replace, clearStack: clearStack, transition: TransitionType.native).then((result){
      // 页面返回result为null
      if (result == null){
        return;
      }
      function(result);
    }).catchError((error) {
      print("$error");
    });
  }

  /// 跳到WebView页
  static goWebViewPage(BuildContext context, String title, String url){
    FocusScope.of(context).unfocus();
    Navigator.of(context)
      .push(new FadeAnimationMaterialPageRoute(builder: (_) {
      return new Browser(
        url: url,
        title: title,
      );
    }));
    //fluro 不支持传中文,需转换
    // push(context, '${Routes.webview}?title=${Uri.encodeComponent(title)}&url=${Uri.encodeComponent(url)}');
  }
}

class FadeAnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  FadeAnimationMaterialPageRoute({
    @required WidgetBuilder builder,    
  }) : super(builder: builder,);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {    
    return OpenUpwardsPageTransitionsBuilder().buildTransitions(this, context, animation, secondaryAnimation, child);    
  }
}