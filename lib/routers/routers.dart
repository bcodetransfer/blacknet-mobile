export 'package:blacknet/routers/arguments.dart';
export 'package:blacknet/routers/utils.dart';

export 'package:blacknet/pages/wallet/router.dart';
export 'package:blacknet/pages/account/router.dart';

import 'package:fluro/fluro.dart';

// modules routers
import 'package:blacknet/pages/wallet/router.dart';
import 'package:blacknet/pages/account/router.dart';
// page
import 'package:blacknet/pages/index.dart';
import 'package:blacknet/pages/me/about.dart';
import 'package:blacknet/widgets/browser.dart';
import 'package:blacknet/pages/theme/index.dart';

import 'package:blacknet/routers/router_init.dart';

class Routes {
  static Router router;

  static String index = "/";
  static String theme = "/theme";
  static String language = "/language";
  static String about = "/about";
  static String signMessage = "/signMessage";
  static String qrpage = "/qrpage";
  static String verifyMessage = "/verifyMessage";
  static String webview = "/webview";

  static String accountBackup = "/account/backup";
  static String accountImport1 = "/account/import/1";
  static String accountImport2 = "/account/import/2";
  static String accountImport3 = "/account/import/3";
  // mainhome
  static String mainPage = "/main";
  // modules router list
  static List<IRouterProvider> _listRouter = [];

  static void generate(Router router) {
    Routes.router = router;
    // main
    router.define(mainPage, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return new MainPage();
    }));
    // about
    router.define(about, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return AboutPage();
    }));
    router.define(accountBackup, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      String mnemonic = params['mnemonic']?.first;
      String address = params['address']?.first;
      String password = params['password']?.first;
      return new AccountBackupPage(
        mnemonic: mnemonic,
        address: address,
        password: password
      );
    }));
    router.define(theme, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return ThemePage();
    }));
    router.define(language, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return LanguagePage();
    }));
    // 定义路由
    router.define(Routes.index, transitionType: TransitionType.native, handler: Handler(handlerFunc: (context, params) {
      return IndexPage();
    }));
    // webview
    router.define(webview, handler: Handler(handlerFunc: (_, params){
      String title = params['title']?.first;
      String url = params['url']?.first;
      return new Browser(
        url: url,
        title: title,
      );
    }));


    // init modules routers
    _listRouter.clear();
    /// 各自路由由各自模块管理，统一在此添加初始化
    _listRouter.add(WalletRouter());
    _listRouter.add(AccountRouter());
    /// 初始化路由
    _listRouter.forEach((routerProvider) {
      routerProvider.initRouter(router);
    });

  }
}