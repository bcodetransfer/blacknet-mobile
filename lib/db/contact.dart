import 'package:blacknet/models/index.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class ContactDbHelper {

  static final ContactDbHelper _instance = new ContactDbHelper.internal();

  factory ContactDbHelper() => _instance;

	static Database _db;                // Singleton Database

	String dbTable = 'contact_table';
	String colAddress = 'address';
	String colName = 'name';
  String colRemark = 'remark';
  String colId = 'id';

  ContactDbHelper.internal();

	Future<Database> get db async {
		if (_db == null) {
			_db = await initializeDatabase();
		}
		return _db;
	}

	Future<Database> initializeDatabase() async {
    // Get the directory path for both Android and iOS to store database.
		Directory directory = await getApplicationDocumentsDirectory();
		String path = directory.path + '/blacknet10000.db';
    // await deleteDatabase(path);
		// Open/create the database at a given path
		Database dbs = await openDatabase(path, version: 1, onCreate: _createDb);
    return dbs;
	}

	void _createDb(Database _db, int newVersion) async {
    await _db.execute('CREATE TABLE $dbTable ($colId INTEGER PRIMARY KEY AUTOINCREMENT, '
				'$colName TEXT, $colAddress TEXT, $colRemark TEXT)');
  }

  // insertUpdate Operation
	Future<int> insertUpdate(BlnContact tx) async {
    BlnContact t = await one(tx.address);
    if(t != null){
      //update
      return await update(tx);
    }else{
      //insert
      return await insert(tx);
    }
	}

  // Insert Operation
	Future<int> insert(BlnContact tx) async {
		Database client = await this.db;
    return await client.insert(dbTable, tx.toMap());
	}

  // one Operation
	Future<BlnContact> one(String txid) async {
		Database client = await this.db;
    if(txid == null){
      txid = "";
    }
    List<Map> result = await client.query(dbTable,
        where: '$colAddress = ?',
        whereArgs: [txid]);

    if (result.length > 0) {
      return BlnContact.fromMap(result.first);
    }
    return null;
	}

  // query Operation
	Future<List<BlnContact>> query(String address, [int page]) async {
		Database client = await this.db;
    String where = '$colAddress = ?';
    List whereArgs = [address];
    int offset = 0;
    if(page != null){
      offset = (page - 1) * 100;
    }
		List<Map> result = await client.query(dbTable,
        where: where,
        whereArgs: whereArgs,
        limit: 100,
        offset: offset
    );
    List<BlnContact> txs = [];
    if (result.length > 0) {
      result.forEach((r){
        txs.add(BlnContact.fromMap(r));
      });
    }
    return txs;
	}

  // count Operation
	Future<int> count() async {
		Database client = await this.db;
    String sql = 'SELECT COUNT(*) FROM $dbTable';
    return Sqflite.firstIntValue(
        await client.rawQuery(sql));
	}

  // Update Operation: Update a todo object and save it to database
	Future<int> update(BlnContact tx) async {
		Database client = await this.db;
    return await client.update(dbTable, tx.toMap(), where: '$colAddress = ?', whereArgs: [tx.address]);
	}

  // Delete Operation: Delete a object from database
	Future<int> delete(String address) async {
		Database client = await this.db;
    return await client.rawDelete('DELETE FROM $dbTable WHERE $colAddress = $address');
	}

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}