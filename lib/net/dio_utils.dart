import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';

import 'package:blacknet/utils/log.dart';
import 'package:blacknet/constants/index.dart';

import 'error_handle.dart';
import 'intercept.dart';
import 'response.dart';

class DioUtils {

  static final DioUtils _singleton = DioUtils._internal();

  static DioUtils get instance => DioUtils();

  factory DioUtils() {
    return _singleton;
  }

  static Dio _dio;

  Dio getDio() {
    return _dio;
  }

  DioUtils._internal() {
    var options = BaseOptions(
      connectTimeout: 15000,
      receiveTimeout: 15000,
      responseType: ResponseType.plain,
      validateStatus: (status) {
        // 不使用http状态码判断状态，使用AdapterInterceptor来处理（适用于标准REST风格）
        return true;
      },
      // baseUrl: 'https://api.github.com/',
      // contentType: ContentType('application', 'x-www-form-urlencoded', charset: 'utf-8'),
      headers: {'Accept': 'application/json, text/plain, */*'}
    );
    _dio = Dio(options);
    /// Fiddler抓包代理配置 https://www.jianshu.com/p/d831b1f7c45b
    //    (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
    //        (HttpClient client) {
    //      client.findProxy = (uri) {
    //        //proxy all request to localhost:8888
    //        return 'PROXY 10.41.0.132:8888';
    //      };
    //      client.badCertificateCallback =
    //          (X509Certificate cert, String host, int port) => true;
    //    };
    /// 统一添加身份验证请求头
    // _dio.interceptors.add(AuthInterceptor());
    /// 刷新Token
    // _dio.interceptors.add(TokenInterceptor());
    /// 适配数据(根据自己的数据结构，可自行选择添加)
    // _dio.interceptors.add(AdapterInterceptor());
    /// 打印Log(生产模式去除)
    if (!Constants.inProduction) {
      _dio.interceptors.add(LoggingInterceptor());
    }
  }

  // 数据返回格式统一，统一处理异常
  Future<GeneralResponse> _request(String method, String url, {
    dynamic data, Map<String, dynamic> queryParameters,
    CancelToken cancelToken, Options options
  }) async {
    var response = await _dio.request(url, data: data, queryParameters: queryParameters, options: _checkOptions(method, options), cancelToken: cancelToken);
    try {
      return GeneralResponse(response.statusCode, response.data.toString());
    } catch(_) {
      return GeneralResponse(ExceptionHandle.parse_error, 'data parse error');
    }
  }

  Options _checkOptions(method, options) {
    if (options == null) {
      options = new Options();
    }
    options.method = method;
    return options;
  }

  Future requestNetwork(Method method, String url, {
        Function(GeneralResponse t) onSuccess, 
        Function(int code, String msg) onError,
        dynamic params, Map<String, dynamic> queryParameters, 
        CancelToken cancelToken, Options options, bool isList : false
  }) {
    String m = _getRequestMethod(method);
    return _request(m, url,
        data: params,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken).then((GeneralResponse result) {
        if (onSuccess != null) {
              onSuccess(result);
        }else {
          _onError(result.code, result.body, onError);
        }
    }, onError: (e, _) {
      _cancelLogPrint(e, url);
      GeneralResponse error = ExceptionHandle.handleException(e);
      _onError(error.code, error.body, onError);
    });
  }

  /// 统一处理(onSuccess返回T对象，onSuccessList返回List<T>)
  asyncRequestNetwork(Method method, String url, {
    Function(GeneralResponse t) onSuccess, 
    Function(int code, String msg) onError,
    dynamic params, Map<String, dynamic> queryParameters, 
    CancelToken cancelToken, Options options, bool isList : false
  }) {
    String m = _getRequestMethod(method);
    Observable.fromFuture(_request(m, url, data: params, queryParameters: queryParameters, options: options, cancelToken: cancelToken))
        .asBroadcastStream()
        .listen((result) {
        if (onSuccess != null) {
              onSuccess(result);
        }else {
          _onError(result.code, result.body, onError);
        }
    }, onError: (e) {
      _cancelLogPrint(e, url);
      GeneralResponse error = ExceptionHandle.handleException(e);
      _onError(error.code, error.body, onError);
    });
  }

  _cancelLogPrint(dynamic e, String url) {
    if (e is DioError && CancelToken.isCancel(e)) {
      Log.e('取消请求接口： $url');
    }
  }

  _onError(int code, String msg, Function(int code, String mag) onError) {
    if (code == null) {
      code = ExceptionHandle.unknown_error;
      msg = '未知异常';
    }
    Log.e('接口请求异常： code: $code, mag: $msg');
    if (onError != null) {
      onError(code, msg);
    }
  }

  String _getRequestMethod(Method method) {
    String m;
    switch(method) {
      case Method.GET:
        m = 'GET';
        break;
      case Method.POST:
        m = 'POST';
        break;
      case Method.PUT:
        m = 'PUT';
        break;
      case Method.PATCH:
        m = 'PATCH';
        break;
      case Method.DELETE:
        m = 'DELETE';
        break;
      case Method.HEAD:
        m = 'HEAD';
        break;
    }
    return m;
  }
}

Map<String, dynamic> parseData(String data) {
  return json.decode(data);
}

enum Method {
  GET,
  POST,
  PUT,
  PATCH,
  DELETE,
  HEAD
}