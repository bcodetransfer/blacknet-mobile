class GeneralResponse {
  int code;
  String body;
  GeneralResponse(this.code, this.body);
  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"code\":\"$code\"");
    sb.write(",\"body\":\"$body\"");
    sb.write('}');
    return sb.toString();
  }
}