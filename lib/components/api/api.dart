import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:blacknet/constants/index.dart';
import 'package:http/http.dart' as http;
import 'package:cookie_jar/cookie_jar.dart';
import 'package:blacknet/models/index.dart';
import 'package:blacknet/constants/resources.dart';
import 'package:dio/dio.dart';

import 'package:blacknet_lib/blacknet.dart';

class Api {
  static Blacknet blacknet = new Blacknet(endpoint: Resources.getApi());
  // pos hashrate
  static Future<PosWorker> getPosWorker(String address) async {
    
    PosWorker worker;

    String url = Resources.getPosApi() + "/api/worker/"+ address;

    var data = await Api.getRequest(url);

    if (data.code == HttpStatus.ok) {
      worker = PosWorker.fromJson(Api.decodeResponse(data.body));
    }
    return worker;
  }
  // pos hashrate
  static Future<PoolHashrate> getPoolHashrate() async {
    
    PoolHashrate rate;

    String url = Resources.getPosApi() + "/api/website_data";

    var data = await Api.getRequest(url);

    if (data.code == HttpStatus.ok) {
      rate = PoolHashrate.fromJson(Api.decodeResponse(data.body));
    }
    return rate;
  }
  // hashrate
  static Future<Hashrate> getHashrate() async {
    
    Hashrate rate;

    String url = Resources.getTxApi() + "/api/networkhashrate";

    var data = await Api.getRequest(url);

    if (data.code == HttpStatus.ok) {
      rate = Hashrate.fromJson(Api.decodeResponse(data.body));
    }
    return rate;
  }
  // pos list
  static Future<List<PosListItem>> getPosLists(String address) async {
    
    List<PosListItem> lists = [];

    String url = Resources.getPosApi() + "/api/account/" + address;

    var data = await Api.getRequest(url);

    if (data.code == HttpStatus.ok) {
      var datas = Api.decodeResponse(data.body);
      var list = datas as List;
      lists = list.map((i) => PosListItem.fromJson(i)).toList();
    }
    return lists;
  }
  // transfer
  static Future<GeneralResponse> transfer(String mnemonic, double fee,
      double amount, String to, bool encrypted, String message) async {

    var bln = Bln.mnemonic(mnemonic);
    if(encrypted) message = Blacknet.encrypt(mnemonic, bln.address, message);
    var res = await blacknet.jsonrpc.transfer(mnemonic, bln.address, to, amount, fee, message, encrypted);

    return GeneralResponse(res.code, res.body);
  }

  // cancelLease
  static Future<GeneralResponse> cancelLease(
      String mnemonic, double fee, double amount, String to, int height) async {

    var bln = Bln.mnemonic(mnemonic);
    var res = await blacknet.jsonrpc.cancelLease(mnemonic, bln.address, to, height, amount, fee);

    return GeneralResponse(res.code, res.body);
  }

  // lease
  static Future<GeneralResponse> lease(
      String mnemonic, double fee, double amount, String to) async {

    var bln = Bln.mnemonic(mnemonic);
    var res = await blacknet.jsonrpc.lease(mnemonic, bln.address, to, amount, fee);

    return GeneralResponse(res.code, res.body);
  }

  // // isstaking
  // static Future<bool> isStaking(String mnemonic) async{
  //   String url = Resources.getApi() + "/api/v2/isstaking";
  //   Map json = Map();
  //   json["mnemonic"] = mnemonic;
  //   GeneralResponse data = await Api.postRequest(url, json);
  //   if (data.code == HttpStatus.ok && data.body == "true") {
  //     return true;
  //   } else {
  //     print(data);
  //   }
  //   return false;
  // }
  // // start staking
  // static Future<bool> startStaking(String mnemonic) async{
  //   String url = Resources.getApi() + "/api/v2/startstaking";
  //   Map json = Map();
  //   json["mnemonic"] = mnemonic;
  //   GeneralResponse data = await Api.postRequest(url, json);
  //   if (data.code == HttpStatus.ok && data.body == "true") {
  //     return true;
  //   } else {
  //     print(data);
  //   }
  //   return false;
  // }
  // // stop staking
  // static Future<bool> stopStaking(String mnemonic) async{
  //   String url = Resources.getApi() + "/api/v2/stopstaking";
  //   Map json = Map();
  //   json["mnemonic"] = mnemonic;
  //   GeneralResponse data = await Api.postRequest(url, json);
  //   if (data.code == HttpStatus.ok && data.body == "true") {
  //     return true;
  //   } else {
  //     print(data);
  //   }
  //   return false;
  // }
  // transfer
  static Future<List<BlnTxns>> getTxns(String address, int page,
      [int type]) async {
    
    List<BlnTxns> txns = [];

    String url = Resources.getTxApi() + "/api/account/txns/" + address;

    url = url + "?page=" + page.toString();

    if (type != null && type != -1) {
      url = url + "&type=" + type.toString();
    }
    
    var data = await Api.getRequest(url);

    if (data.code == HttpStatus.ok) {
      var datas = Api.decodeResponse(data.body);
      var list = datas as List;
      txns = list.map((i) => BlnTxns.fromJson(i)).toList();
    }
    return txns;
  }


  // outlease
  static Future<List<BlnLease>> outLeases(String address) async {
    List<BlnLease> leases = [];

    String url = Resources.getTxApi() + "/api/outleases/" + address;
    var data = await Api.getRequest(url);
    if (data.code == HttpStatus.ok) {
      var datas = Api.decodeResponse(data.body);
      var list = datas as List;
      leases = list.map((i) => BlnLease.fromJson(i)).toList();
    }
    return leases;
  }

  // 用户余额
  static Future<BlnBalance> getBlance(String address,
      [String confirmations]) async {
    BlnBalance balance = new BlnBalance(
        seq: 0, confirmedBalance: 0, stakingBalance: 0, balance: 0);
    // https://www.blnscan.io/api/account/blacknet17x3w4356axe7k9nyy8nvpe5z3e0qxgkycjj4zlvmrs9p28r7587qrn5u8u?type=3

    String url = Resources.getTxApi() + "/api/account/ledger/" + address;
    // if( confirmations != null ){
    //   url = url +"/"+ confirmations;
    // }
    await Api.getRequest(url).then((data) {
      if (data.code == HttpStatus.ok) {
        var fields = Api.decodeResponse(data.body);
        balance = BlnBalance.fromJson(fields);
      } else {
        print(data.body);
      }
    });
    return balance;
  }

  // tx
  static Future<BlnTxns> getTransaction(String txhash,
      [String confirmations]) async {
    BlnTxns tx = new BlnTxns();
    String url =
        Resources.getApi() + "/api/v1/walletdb/gettransaction/" + txhash;
    if (confirmations != null) {
      url = url + "/" + confirmations;
    }
    await Api.getRequest(url).then((data) {
      if (data.code == HttpStatus.ok) {
        var fields = Api.decodeResponse(data.body);
        tx = BlnTxns.fromJson(fields);
      } else {
        print(data.body);
      }
    });
    return tx;
  }

  // tx
  static Future<BlnTxns> getTx(String txhash, [String confirmations]) async {
    BlnTxns tx = new BlnTxns();
    String url = Resources.getTxApi() + "/api/tx/" + txhash;
    if (confirmations != null) {
      url = url + "/" + confirmations;
    }
    await Api.getRequest(url).then((data) {
      if (data.code == HttpStatus.ok) {
        try {
          var fields = Api.decodeResponse(data.body);
          tx = BlnTxns.fromJson(fields);
        } catch (e) {
          data.code = 400;
        }
      } else {
        print(data.body);
      }
    });
    return tx;
  }

  static Future<GeneralResponse> httpGet(String url) async {
    print(url);
    Uri uri = Uri.parse(url);
    var client = new http.Client();
    try {
      var response = await client.get(uri);
      return GeneralResponse(response.statusCode, response.body);
    } on TimeoutException catch (_) {
      return GeneralResponse(600, "connection timeout");
    } on HandshakeException catch (_) {
      return GeneralResponse(601, "error handshake");
    } on SocketException catch (_) {
      return GeneralResponse(602, "error handshake");
    } finally {
      client.close();
    }
  }

  static Future<GeneralResponse> getRequest(String url) async {
    print(url);
    Uri uri = Uri.parse(url);
    CookieJar cj = new CookieJar();
    HttpClient httpClient = new HttpClient();
    try {
      HttpClientRequest request = await httpClient.getUrl(uri);
      // 获取cookie并添加header
      request.cookies.addAll(cj.loadForRequest(uri));
      request.headers.add('Accept', 'application/json, text/plain, */*');
      HttpClientResponse response = await request.close();
      //Save cookies
      cj.saveFromResponse(uri, response.cookies);
      // utf8
      String reply = await response.transform(utf8.decoder).join();
      return GeneralResponse(response.statusCode, reply);
    } on TimeoutException catch (_) {
      return GeneralResponse(600, "connection timeout");
    } on HandshakeException catch (_) {
      return GeneralResponse(601, "error handshake");
    } on SocketException catch (_) {
      return GeneralResponse(602, "error handshake");
    } finally {
      httpClient.close();
    }
  }

  static Future<GeneralResponse> postRequest(String url, [Map jsonMap]) async {
    print(url);
    Uri uri = Uri.parse(url);
    CookieJar cj = new CookieJar();
    HttpClient httpClient = new HttpClient();
    try {
      HttpClientRequest request = await httpClient.postUrl(uri);
      // 获取cookie并添加header
      request.cookies.addAll(cj.loadForRequest(uri));
      // data
      request.headers.set('content-type', 'application/json');
      if (null != jsonMap) {
        request.add(utf8.encode(json.encode(jsonMap)));
      }
      HttpClientResponse response = await request.close();
      //Save cookies
      cj.saveFromResponse(uri, response.cookies);
      // utf8
      String reply = await response.transform(utf8.decoder).join();
      return GeneralResponse(response.statusCode, reply);
    } on TimeoutException catch (_) {
      return GeneralResponse(600, "connection timeout");
    } on HandshakeException catch (_) {
      return GeneralResponse(601, "error handshake");
    } on SocketException catch (_) {
      return GeneralResponse(602, "error handshake");
    } finally {
      httpClient.close();
    }
  }

  static Future<GeneralResponse> postForm(String url, [FormData form]) async {
    print(url);
    var dio = new Dio();
    try {
      FormData formData = new FormData.from({});
      if (form != null) {
        formData = form;
      }
      var response = await dio.post(url, data: formData);
      return GeneralResponse(response.statusCode, response.data.toString());
    } on TimeoutException catch (_) {
      return GeneralResponse(600, "connection timeout");
    } on HandshakeException catch (_) {
      return GeneralResponse(601, "error handshake");
    } on SocketException catch (_) {
      return GeneralResponse(602, "error handshake");
    } finally {
      dio.clear();
    }
  }

  static Future<GeneralResponse> dioGet(String url,
      [Map<String, dynamic> params]) async {
    print(url);
    var dio = new Dio(
        BaseOptions(headers: {'Accept': 'application/json, text/plain, */*'}));
    try {
      print(params);
      var response = await dio.get(url, queryParameters: params);
      print(response.statusCode);
      print(response.data);
      return GeneralResponse(response.statusCode, response.data.toString());
    } on TimeoutException catch (_) {
      return GeneralResponse(600, "connection timeout");
    } on HandshakeException catch (_) {
      return GeneralResponse(601, "error handshake");
    } on SocketException catch (_) {
      return GeneralResponse(602, "error handshake");
    } finally {
      dio.clear();
    }
  }

  static decodeResponse(String body) {
    var result = json.decode(body);
    return result;
  }
}
