
class Resources {
  static String localApi = "http://127.0.0.1:8283";
  static String internetApi = "https://blnmobiledaemon.blnscan.io";
  static String txApi = "https://blnscan.io";
  static String posApi = "https://blnpool.io";
  static String getTxApi(){
    return Resources.txApi;
  }
  static String getApi(){
    return Resources.internetApi;
    // return Resources.localApi;
  }
  static String getPosApi(){
    return Resources.posApi;
  }
}



// 枚举功能方法
enum EnumFunc {
  transfer, // 转账
  lease,
  keystore,
  staking
}

// 枚举钱包
enum EnumWallet {
  create,
  import
}

// 枚举恢复方式
enum EnumAccount {
  mnemonic,
  privatekey,
  keystore,
  observe
}

// 枚举status
enum EnumStatus {
  all,
  transfer,
  lease,
  cancellease,
  pos
}
