import 'package:blacknet/utils/aes.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('AES test', (){
    ///
    /// AES
    ///
    final String matcher = "QnZqC8IqoEnk1a1c76fGHZvUtoVLkq21W1PiSvUNIHg=";
    final String plainText = "blacknet is perfect";
    final String keyText = "00000000000x7c00";
    var encrypted = AESUtil.encryptToBase64(plainText, keyText);
    expect(encrypted, matcher);
    var decrypted = AESUtil.decryptFromBase64(encrypted, keyText);
    expect(decrypted, plainText);
  });
}
