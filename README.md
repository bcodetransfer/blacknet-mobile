# Blacknet

[Mobile wallet](https://gitlab.com/blacknet-ninja/blacknet-mobile/-/releases) for Blacknet.


# Get Started

## 0 env ready

```
https://flutter.dev/docs/get-started/install
```

## 1 run
```
flutter run
```

## 2 build
```
flutter build apk 
```

