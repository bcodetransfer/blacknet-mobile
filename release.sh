
flutter build apk

mobile_version=$(cat pubspec.yaml|grep version: | awk -F:  '{print $2}' |sed -e 's/ //g')

echo "Start release $mobile_version"

cp build/app/outputs/apk/release/app-release.apk app/blacknet-mobile-$mobile_version.apk

response=`curl --request POST --header "Private-Token: ${gitlab_private_token}" --form "file=@app/blacknet-mobile-${mobile_version}.apk" "https://gitlab.COM/api/v4/projects/blacknet-ninja%2Fblacknet-mobile/uploads"`

markdown=`echo $response | jq -r '.markdown'`

echo $markdown